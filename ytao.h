// ytao.h -
//
// Definitions for Yorick interface to TAO real-time software.
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#ifndef YTAO_H_
#define YTAO_H_ 1

#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

#include <tao-basics.h>
#include <tao-encodings.h>
#include <tao-errors.h>
#include <tao-remote-cameras.h>
#include <tao-remote-mirrors.h>
#include <tao-remote-objects.h>
#include <tao-remote-sensors.h>
#include <tao-rwlocked-objects.h>
#include <tao-shared-arrays.h>
#include <tao-shared-memory.h>
#include <tao-shared-objects.h>

#include <pstdlib.h>
#include <play.h>
#include <yapi.h>
#include <ydata.h>

// Define some macros to get rid of some GNU extensions when not compiling
// with GCC.
#if ! (defined(__GNUC__) && __GNUC__ > 1)
#   define __attribute__(x)
#   define __inline__
#   define __FUNCTION__        ""
#   define __PRETTY_FUNCTION__ ""
#endif

extern void y_error(const char *) TAO_NORETURN;

// Address of TAO last error.
extern tao_error* ytao_last_error;

// Throw last TAO library error.
extern void ytao_throw_last_error(void) TAO_NORETURN;

// Throw formatted error message.
extern void ytao_throw(const char* mesg, ...)
    TAO_FORMAT_PRINTF(1,2) TAO_NORETURN;

// Copy source array `src` of Yorick type `src_type` (Y_CHAR, Y_SHORT, etc.)
// into a destination array `dst` of TAO type `dst_type`.  Argument `ntot` is
// the number of elements.
extern void ytao_copy(
    void*       dst,
    tao_eltype  dst_type,
    const void* src,
    int         src_type,
    long ntot);

// Push a string.
static inline void ytao_push_string(const char* str_)
{
    ypush_q(NULL)[0] = p_strcpy(str_);
}

// Push an array of integers of given element size and dimensions.
extern void* ytao_push_integers(size_t elsize, long dims[]);

// Push an array of floating-point values of given element size and dimensions.
extern void* ytao_push_floats(size_t elsize, long dims[]);

// Push a pair of serial numbers.
extern tao_serial *ytao_serial_pair_push(
    tao_serial first,
    tao_serial second);

// Get a serial number from elemnt `iarg` of the stack.  Argument `which` must
// be 0 (for a command serial) or 1 (for the serial number of some output
// data).
extern tao_serial ytao_get_serial_number(int iarg, long which);

//-----------------------------------------------------------------------------
// STACK ARGUMENT

static inline bool ytao_is_scalar(int iarg)
{
    return yarg_rank(iarg) == 0;
}

static inline bool ytao_is_integer(int iarg)
{
    int type = yarg_typeid(iarg);
    return (type >= Y_CHAR && type <= Y_LONG);
}

static inline bool ytao_is_real(int iarg)
{
    int type = yarg_typeid(iarg);
    return (type >= Y_CHAR && type <= Y_DOUBLE);
}

static inline bool ytao_is_string(int iarg)
{
    return yarg_typeid(iarg) == Y_STRING;
}

static inline bool ytao_is_scalar_integer(int iarg)
{
    return ytao_is_scalar(iarg) && ytao_is_integer(iarg);
}

static inline bool ytao_is_scalar_real(int iarg)
{
    return ytao_is_scalar(iarg) && ytao_is_real(iarg);
}

static inline bool ytao_is_scalar_string(int iarg)
{
    return ytao_is_scalar(iarg) && ytao_is_string(iarg);
}

//-----------------------------------------------------------------------------
// TIME-STAMPS AND ORIGIN OF TIME

extern tao_time ytao_monotonic_origin;

// Push elapsed time since origin of monotonic time.
inline double ytao_elapsed_time(const tao_time* t)
{
    return (double)(t->sec - ytao_monotonic_origin.sec) +
        1e-9*(double)(t->nsec - ytao_monotonic_origin.nsec);
}

// Push elapsed time since origin of monotonic time.
inline void ytao_push_elapsed_time(const tao_time* t)
{
    ypush_double(ytao_elapsed_time(t));
}

// Push time as a pair of integers.
static inline void ytao_push_time(const tao_time* t)
{
    long dims[2] = {1, 2};
    long* ptr = ypush_l(dims);
    ptr[0] = t->sec;
    ptr[1] = t->nsec;
}

//-----------------------------------------------------------------------------
// PRIVATE RESOURCES.

// Dynamic buffer.
extern tao_buffer ytao_dynbuf;

// Check the validity of a TAO array element type assuming TAO_INT8 and
// TAO_DOUBLE are the least and greatest possible values.
#define YTAO_CHECK_TAO_ELTYPE(eltype) \
    ((unsigned)((eltype) - TAO_INT8) <= (unsigned)(TAO_DOUBLE - TAO_INT8))

// Check the validity of a Yorick arry element type assuming Y_CHAR and
// Y_DOUBLE are the least and the greatest possible values.
#define YTAO_CHECK_YORICK_ELTYPE(eltype) \
    ((unsigned)((eltype) - Y_CHAR) <= (unsigned)(Y_DOUBLE - Y_CHAR))

// Get Yorick array element type corresponding to a given TAO array element
// type.  The argument must have been checked by YTAO_CHECK_TAO_ELTYPE.
extern int const* ytao_to_yorick_eltype_;
#define YTAO_TO_YORICK_ELTYPE(eltype) ytao_to_yorick_eltype_[eltype]

// Get TAO array element type corresponding to a given Yorick array element
// type.  The argument must have been checked by YTAO_CHECK_YORICK_ELTYPE.
extern int const* ytao_to_tao_eltype_;
#define YTAO_TO_TAO_ELTYPE(eltype) ytao_to_tao_eltype_[eltype]

//-----------------------------------------------------------------------------
// YORICK OBJECTS WRAPPING TAO OBJECTS

// Lock mode.
typedef enum {
    YTAO_LOCK_NONE     = 0,
    YTAO_LOCK_READONLY  = 1,
    YTAO_LOCK_READWRITE = 2,
} ytao_lock_state;

// Opaque definitions of shared object and table of operations.
typedef struct ytao_object     ytao_object;
typedef struct ytao_operations ytao_operations;

// Internal representation of a TAO shared object for Yorick.  Some (immutable)
// information are copied (to save calling getters) and `lock` is used to keep
// track of whether the object is locked by Yorick or not (this is exploited to
// prevent deadlocks and other similar issues).
struct ytao_object {
    void*               shared; // pointer to a TAO object in shared memory
    const ytao_operations* ops; // table of operations
    tao_shmid            shmid; // shared memory identifier
    size_t                size; // total size in bytes
    const char*          owner; // name of server owning the object
    uint32_t              type; // type identifier
    ytao_lock_state       lock; // current lock state
    void*              private; // private data associated with object
};

// Table of operations on a shared object.
struct ytao_operations {
    bool rwlocked;
    void (*on_eval)(ytao_object* yobj, int argc);
    void (*on_extract)(ytao_object* yobj, const char* name);
    void (*on_wait_data_frame)(ytao_object* yobj, tao_serial serial,
                               double secs, bool subroutine);
};

extern y_userobj_t ytao_object_type;

// Retrieve shared object from arguments of built-in function.
// If `iarg < 0`, a missing argument is assumed.
inline ytao_object* ytao_get_object(int iarg)
{
    if (iarg < 0) y_error("missing TAO object argument");
    return (ytao_object*)yget_obj(iarg, &ytao_object_type);
}

//-----------------------------------------------------------------------------
// SHARED OBJECTS

extern ytao_operations ytao_shared_object_ops;


//-----------------------------------------------------------------------------
// RWLOCKEDD OBJECTS

extern ytao_operations ytao_rwlocked_object_ops;


//-----------------------------------------------------------------------------
// SHARED ARRAYS

extern ytao_operations ytao_shared_array_ops;

inline void ytao_push_shared_array_timestamp(tao_shared_array* arr, int idx)
{
    tao_time t;
    tao_shared_array_get_timestamp(arr, idx, &t);
    ytao_push_elapsed_time(&t);
}

inline tao_shared_array* ytao_get_shared_array(int iarg)
{
    ytao_object* yobj = ytao_get_object(iarg);
    if (yobj->ops != &ytao_shared_array_ops) {
        y_error("TAO shared object is not an array");
    }
    return (tao_shared_array*)yobj->shared;
}

//-----------------------------------------------------------------------------
// REMOTE OBJECTS

extern ytao_operations ytao_remote_object_ops;

//-----------------------------------------------------------------------------
// REMOTE CAMERAS

extern ytao_operations ytao_remote_camera_ops;

inline tao_remote_camera* ytao_get_remote_camera(int iarg)
{
    ytao_object* yobj = ytao_get_object(iarg);
    if (yobj->ops != &ytao_remote_camera_ops) {
        y_error("TAO shared object is not a camera");
    }
    return (tao_remote_camera*)yobj->shared;
}

//-----------------------------------------------------------------------------
// REMOTE MIRRORS

extern ytao_operations ytao_remote_mirror_ops;

//-----------------------------------------------------------------------------
// REMOTE SENSORS

extern ytao_operations ytao_remote_sensor_ops;

#endif // YTAO_H_ -------------------------------------------------------------
