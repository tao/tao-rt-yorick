// ytao_remote_object.c -
//
// Yorick interface to TAO remote objects.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-remote-objects-private.h>

static void remote_object_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    const tao_remote_object* obj = yobj->shared;
    switch (name[0]) {
    case 'a':
        if (match("alive", 5)) {
            ypush_int(tao_remote_object_is_alive(obj));
            return;
        }
        break;
    case 'f':
        if (match("flags", 5)) {
            ypush_long(tao_remote_object_get_flags(obj));
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        break;
    case 'n':
        if (match("nbufs", 5)) {
            ypush_long(tao_remote_object_get_nbufs(obj));
            return;
        }
        if (match("ncmds", 5)) {
            ypush_long(tao_remote_object_get_ncmds(obj));
            return;
        }
        if (match("nrefs", 5)) {
            ypush_long(obj->base.nrefs);
            return;
        }
        break;
    case 'o':
        if (match("owner", 5)) {
            ytao_push_string(yobj->owner);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            ypush_long(tao_remote_object_get_flags(obj) & 0x1ff);
            return;
        }
        if (match("pid", 3)) {
            ypush_long(tao_remote_object_get_pid(obj));
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(tao_remote_object_get_serial(obj));
            return;
        }
        if (match("state", 5)) {
            ypush_int(tao_remote_object_get_state(obj));
            return;
        }
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        break;
    case 't':
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    }
    y_error("bad member");
}

ytao_operations ytao_remote_object_ops = {
    // TAO_REMOTE_OBJECT,
    false,
    NULL,
    remote_object_extract
};
