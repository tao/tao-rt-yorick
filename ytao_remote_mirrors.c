// ytao_remote_mirrors.c -
//
// Yorick interface to TAO remote mirrors.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-remote-mirrors-private.h>

//-----------------------------------------------------------------------------
// DEFORMABLE MIRROR DATA FRAME OBJECT

typedef struct mirror_data_frame mirror_data_frame;

struct mirror_data_frame {
    tao_dataframe_info info; // Data frame information.
    long              nacts; // Number of actuators (whatever is in info).
    double          vals[1]; // Reference values and commands
};

static void free_mirror_data_frame(void* addr)
{
    // Nothing to do.
}

static void print_mirror_data_frame(void* addr)
{
    mirror_data_frame* frame = addr;
    tao_buffer_clear(&ytao_dynbuf);
    if (tao_buffer_printf(
            &ytao_dynbuf, "deformable mirror data frame "
            "(nacts=%ld, serial=%lld, mark=%lld, time=%lld.%09lds)",
            (long      int)frame->nacts,
            (long long int)frame->info.serial,
            (long long int)frame->info.mark,
            (long long int)frame->info.time.sec,
            (long      int)frame->info.time.nsec) != TAO_OK) {
        ytao_throw_last_error();
    }
    y_print(tao_buffer_get_contents(&ytao_dynbuf, NULL), 1);
    tao_buffer_clear(&ytao_dynbuf);
}

static void push_mirror_data_frame_values(
    const mirror_data_frame* frame,
    long which)
{
    long nacts = frame->nacts;
    long dims[2] = {1, nacts};
    memcpy(ypush_d(dims), frame->vals + which*nacts, nacts*sizeof(double));
}

static void extract_mirror_data_frame(void* addr, char* name)
{
    size_t len = strlen(name);
    mirror_data_frame* frame = addr;
    switch (name[0]) {
    case 'd':
        if (match("devcmds", 7)) {
            push_mirror_data_frame_values(frame, 3);
            return;
        }
        break;
    case 'm':
        if (match("mark", 4)) {
            ypush_long(frame->info.mark);
            return;
        }
        break;
    case 'n':
        if (match("nacts", 5)) {
            ypush_long(frame->nacts);
            return;
        }
        break;
    case 'p':
        if (match("perturb", 7)) {
            push_mirror_data_frame_values(frame, 1);
            return;
        }
        break;
    case 'r':
        if (match("refcmds", 7)) {
            push_mirror_data_frame_values(frame, 0);
            return;
        }
        if (match("reqcmds", 7)) {
            push_mirror_data_frame_values(frame, 2);
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(frame->info.serial);
            return;
        }
        break;
    case 't':
        if (match("time", 4)) {
            ytao_push_time(&frame->info.time);
            return;
        }
        break;
    }
    y_error("bad member");
}

static y_userobj_t mirror_data_frame_type = {
    "tao_mirror_data_frame",
    free_mirror_data_frame,
    print_mirror_data_frame,
    NULL, // on_eval
    extract_mirror_data_frame,
    NULL
};

static mirror_data_frame* push_mirror_data_frame(long nacts)
{
    size_t bufsiz = nacts*sizeof(double);
    size_t objsiz = offsetof(mirror_data_frame, vals) + 4*bufsiz;
    mirror_data_frame* frame = ypush_obj(&mirror_data_frame_type, objsiz);
    frame->nacts = nacts;
    return frame;
}

#if 0 // FIXME: unused
static mirror_data_frame* get_mirror_data_frame(int iarg)
{
    if (iarg < 0) y_error("missing mirror data frame argument");
    return yget_obj(iarg, &mirror_data_frame_type);
}
#endif

//-----------------------------------------------------------------------------
// REMOTE MIRROR OBJECT

static void remote_mirror_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    const tao_remote_mirror* obj = yobj->shared;
    switch (name[0]) {
    case 'a':
        if (match("alive", 5)) {
            ypush_int(tao_remote_mirror_is_alive(obj));
            return;
        }
        break;
    case 'c':
        if (match("cmin", 4)) {
            ypush_long(tao_remote_mirror_get_cmin(obj));
            return;
        }
        if (match("cmax", 4)) {
            ypush_long(tao_remote_mirror_get_cmax(obj));
            return;
        }
        break;
    case 'd':
        if (match("dims", 4)) {
            long dims[2] = {1,3};
            long* dst = ypush_l(dims);
            const long* src = tao_remote_mirror_get_dims(obj);
            dst[0] = 2;
            dst[1] = src[0];
            dst[2] = src[1];
            return;
        }
        break;
    case 'f':
        if (match("flags", 5)) {
            ypush_long(tao_remote_mirror_get_flags(obj));
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        if (match("layout", 6)) {
            long dims[3];
            const long* src = tao_remote_mirror_get_layout(obj, dims+1);
            dims[0] = 2;
            if (src != NULL && dims[1] > 0 && dims[2] > 0) {
                long* dst = ypush_l(dims);
                long n = dims[1]*dims[2];
                for (long i = 0; i < n; ++i) {
                    // Convert 0-based indices into 1-based indices.
                    dst[i] = src[i] + 1;
                }
            } else {
                ypush_nil();
            }
            return;
        }
        break;
    case 'm':
        if (match("mark", 4)) {
            ypush_long(tao_remote_mirror_get_mark(obj));
            return;
        }
        break;
    case 'n':
        if (match("nacts", 5)) {
            ypush_long(tao_remote_mirror_get_nacts(obj));
            return;
        }
        if (match("nbufs", 5)) {
            ypush_long(tao_remote_mirror_get_nbufs(obj));
            return;
        }
        if (match("ncmds", 5)) {
            ypush_long(tao_remote_mirror_get_ncmds(obj));
            return;
        }
        if (match("nrefs", 5)) {
            ypush_long(obj->base.base.nrefs);
            return;
        }
        break;
    case 'o':
        if (match("owner", 5)) {
            ytao_push_string(yobj->owner);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            ypush_long(tao_remote_mirror_get_flags(obj) & 0x1ff);
            return;
        }
        if (match("pid", 3)) {
            ypush_long(tao_remote_mirror_get_pid(obj));
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(tao_remote_mirror_get_serial(obj));
            return;
        }
        if (match("state", 5)) {
            ypush_int(tao_remote_mirror_get_state(obj));
            return;
        }
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        break;
    case 'r':
        if (match("refcmds", 7)) {
            long nacts = tao_remote_mirror_get_nacts(obj);
            long dims[2] = {1, nacts};
            double* dst = ypush_d(dims);
            const double* src = tao_remote_mirror_get_reference(obj);
            memcpy(dst, src, nacts*sizeof(double));
            return;
        }
        break;
    case 't':
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    }
    y_error("bad member");
}

static void remote_mirror_wait_data_frame(
    ytao_object* yobj,
    tao_serial datnum,
    double secs,
    bool subroutine)
{
    tao_remote_mirror* obj = yobj->shared;
    datnum = tao_remote_mirror_wait_output(obj, datnum, secs);
    if (datnum < -2) {
        // An error occurred.
    error:
        ytao_throw_last_error();
        return;
    }
    if (subroutine) {
        if (datnum < 1) {
            // Frame cannot be retrieved.
            const char* mesg = (datnum == 0 ? "timeout" :
                                (datnum == -1 ? "too late" :
                                 "server has been killed"));
            y_error(mesg);
        }
        return;
    }
    // Retrieve frame contents.
    long nacts = tao_remote_mirror_get_nacts(obj);
    mirror_data_frame* frame = push_mirror_data_frame(nacts);
    if (datnum < 1) {
        // Contents is not available.
        frame->info.serial = datnum;
    } else {
        // Attempt to load contents.
        tao_status status = tao_remote_mirror_fetch_data(
            obj, datnum, frame->vals, frame->vals + nacts,
            frame->vals + 2*nacts, frame->vals + 3*nacts,
            nacts, &frame->info);
        if (status != TAO_OK) {
            if (status == TAO_TIMEOUT) {
                // Contents have been overwritten.
                frame->info.serial = -1;
            } else {
                goto error;
            }
        }
    }
}

ytao_operations ytao_remote_mirror_ops = {
    // TAO_REMOTE_MIRROR,
    false,
    NULL, // on_eval,
    remote_mirror_extract,
    remote_mirror_wait_data_frame
};
