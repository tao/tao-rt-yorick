// ytao_utils.c -
//
// Useful functions for Yorick interface to TAO real-time software.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

tao_error* ytao_last_error = NULL;

// Throw formatted error message.
void ytao_throw(const char* mesg, ...)
{
    static char buf[256];
    va_list args;
    va_start(args, mesg);
    vsnprintf(buf, LENGTH(buf), mesg, args);
    va_end(args);
    buf[LENGTH(buf) - 1] = 0;
    y_error(buf);
}

// Throw last TAO library error.
void ytao_throw_last_error(void)
{
    if (ytao_last_error == NULL) {
        y_error("`_tao_init` must be called to initialize the plug-in");
    }
    char* mesg = NULL;
    if (ytao_last_error->code != TAO_SUCCESS) {
        tao_buffer_clear(&ytao_dynbuf);
        if (tao_report_error_to_buffer(
                &ytao_dynbuf, ytao_last_error, "", "") != TAO_OK) {
            mesg = "error while calling `tao_report_error_to_buffer`";
        } else {
            size_t len = 0;
            mesg = tao_buffer_get_contents(&ytao_dynbuf, &len);
            if (len <= 0) {
                mesg = NULL;
            }
        }
    }
    if (mesg == NULL) {
        mesg = "no TAO last error";
    }
    y_error(mesg);
}

void* ytao_push_integers(size_t elsize, long dims[])
{
    if (elsize == sizeof(char)) {
        return ypush_c(dims);
    }
#if SHRT_MAX > SCHAR_MAX
    if (elsize == sizeof(short)) {
        return ypush_s(dims);
    }
#endif
#if INT_MAX > SHRT_MAX
    if (elsize == sizeof(int)) {
        return ypush_i(dims);
    }
#endif
#if LONG_MAX > INT_MAX
    if (elsize == sizeof(long)) {
        return ypush_l(dims);
    }
#endif
    ytao_throw("no %zu-bit integer type", 8*elsize);
}

void* ytao_push_floats(size_t elsize, long dims[])
{
    if (elsize == sizeof(float)) {
        return ypush_f(dims);
    }
    if (elsize == sizeof(double)) {
        return ypush_d(dims);
    }
    ytao_throw("no %zu-bit floating-point type", 8*elsize);
}

//-----------------------------------------------------------------------------
// PAIR OF SERIAL NUMBERS

typedef struct serial_pair serial_pair;

struct serial_pair {
    tao_serial vals[2];
};

static void serial_pair_print(void* addr)
{
    serial_pair* obj = addr;
    tao_buffer_clear(&ytao_dynbuf);
    if (tao_buffer_printf(
            &ytao_dynbuf, "pair of serial numbers: %lld => %lld",
            (long long int)obj->vals[0],
            (long long int)obj->vals[1]) != TAO_OK) {
        ytao_throw_last_error();
    }
    y_print(tao_buffer_get_contents(&ytao_dynbuf, NULL), 1);
    tao_buffer_clear(&ytao_dynbuf);
}

// Convert Yorick index `i` along dimension of length `n` to a 0-based (C)
// index with the same convention as in Yorick that `u ≤ 0` is taken from the
// end.  Argument (i) is evaluated more than once.
#define ZERO_BASED_INDEX(i, n) ((i) > 0 ? (i) - 1 : (n) - 1 + (i))

static void serial_pair_eval(void* addr, int argc)
{
    serial_pair* obj = addr;
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    int type = yarg_typeid(0);
    if (type <= Y_LONG) {
        if (yarg_rank(0) == 0) {
            // Scalar index.
            long i = ygets_l(0);
            i = ZERO_BASED_INDEX(i, 2);
            if ((unsigned long)i > 1) {
                y_error("array index is out of bounds");
            }
            ypush_long(obj->vals[i]);
        } else {
            long dims[Y_DIMSIZE], ntot;
            const long* J = ygeta_l(0, &ntot, dims);
            long* dst = ypush_l(dims);
            for (long i = 0; i < ntot; ++i) {
                long j = J[i] - 1;
                if ((unsigned long)j > 1) {
                    y_error("array index is out of bounds");
                }
                dst[i] = obj->vals[j];
            }
        }
        return;
     } else if (type == Y_VOID) {
    flat: {
            long dims[2] = {1,2};
            long* arr = ypush_l(dims);
            arr[0] = obj->vals[0];
            arr[1] = obj->vals[1];
            return;
        }
    } else if (type == Y_RANGE) {
        long mms[3]; // min/max/step
        int rng = yget_range(0, mms);
#if 0
        fprintf(stderr, "rng=%d, %ld:%ld:%ld\n",
                rng, mms[0], mms[1], mms[2]);
#endif
        if (rng == RANGE_STAR) {
            // Range is `*`. Yield a flat array like `obj()`.
            goto flat;
        } else if ((rng & 0x0f) == 1) {
            // Range is `mn:mx:stp`.
            long stp = mms[2];
            long mn = ((rng&Y_MIN_DFLT) == 0 ? mms[0] : (stp > 0 ? 1 : 2));
            long mx = ((rng&Y_MAX_DFLT) == 0 ? mms[1] : (stp > 0 ? 2 : 1));
            mn = ZERO_BASED_INDEX(mn, 2);
            mx = ZERO_BASED_INDEX(mx, 2);
            long n = 1 + (mx - mn)/stp;
            if (n <= 0) {
                y_error("array index range step has wrong sign");
            }
            if ((unsigned long)mn > 1) {
                y_error("array index range start is out of bounds");
            }
            if ((unsigned long)mx > 1) {
                y_error("array index range stop is out of bounds");
            }
            long dims[2] = {1, n};
            long* arr = ypush_l(dims);
            for (long i = 0; i < n; ++i) {
                arr[i] = obj->vals[mn + i*stp];
            }
            return;
#if 0
        } else if (rng == RANGE_PLUS) {
            // Range is `+`.
        } else if (rng == RANGE_MINUS) {
            // Range is `-`.
        } else if (rng == RANGE_RUBBER) {
            // Range is `..`.
#endif
        }
    }
    y_error("invalid index for a pair of serial numbers");
}

static void serial_pair_extract(void* addr, char* name)
{
    serial_pair* obj = addr;
    int c = name[0];
    if (c == 'f' && strcmp("first", name) == 0) {
        ypush_long(obj->vals[0]);
    } else if (c == 's' && strcmp("second", name) == 0) {
        ypush_long(obj->vals[1]);
    } else {
        y_error("bad member, expecting `first` or `second`");
    }
}

static y_userobj_t serial_pair_type = {
    "pair of serial numbers",
    NULL, // on_free
    serial_pair_print,
    serial_pair_eval,
    serial_pair_extract,
    NULL
};

tao_serial *ytao_serial_pair_push(
    tao_serial first,
    tao_serial second)
{
    serial_pair* obj = ypush_obj(&serial_pair_type, sizeof(serial_pair));
    obj->vals[0] = first;
    obj->vals[1] = second;
    return obj->vals;
}

tao_serial ytao_get_serial_number(int iarg, long which)
{
    if ((unsigned long)which > 1) {
        y_error("(BUG) unexpected argument `which` in "
                "`ytao_get_serial_number`");
    }
    int type = yarg_typeid(iarg);
    if (type <= Y_LONG) {
        if (yarg_rank(iarg) == 0) {
            return ygets_l(iarg);
        } else {
            long ntot;
            const long* arr = ygeta_l(iarg, &ntot, NULL);
            if (ntot == 2) {
                return arr[which];
            }
        }
    } else {
        const char* type_name = yget_obj(iarg, NULL);
        if (type_name == serial_pair_type.type_name) {
            serial_pair* obj = yget_obj(iarg, &serial_pair_type);
            return obj->vals[which];
        }
    }
    y_error("expecting a serial number of a pair of serial numbers");
}

//-----------------------------------------------------------------------------
