// ytao_remote_cameras.c -
//
// Yorick interface to TAO remote cameras.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-remote-cameras-private.h>

static void remote_camera_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    const tao_remote_camera* cam = yobj->shared;
    switch (name[0]) {
    case 'a':
        if (match("alive", 5)) {
            ypush_int(tao_remote_camera_is_alive(cam));
            return;
        }
        break;
    case 'b':
        if (match("buffers", 7)) {
            ypush_long(cam->config.buffers);
            return;
        }
        break;
    case 'd':
        if (match("droppedframes", 13)) {
            ypush_long(cam->config.droppedframes);
            return;
        }
        break;
    case 'e':
        if (match("exposuretime", 12)) {
            ypush_double(tao_remote_camera_get_exposuretime(cam));
            return;
        }
        break;
    case 'f':
        if (match("framerate", 9)) {
            ypush_double(tao_remote_camera_get_framerate(cam));
            return;
        }
        if (match("flags", 5)) {
            ypush_long(tao_remote_camera_get_flags(cam));
            return;
        }
        if (match("frames", 6)) {
            ypush_long(cam->config.frames);
            return;
        }
        break;
    case 'h':
        if (match("height", 6)) {
            ypush_long(tao_remote_camera_get_height(cam));
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        if (match("lostframes", 10)) {
            ypush_long(cam->config.lostframes);
            return;
        }
        if (match("lostsyncs", 9)) {
            ypush_long(cam->config.lostsyncs);
            return;
        }
        break;
    case 'n':
        if (match("nbufs", 5)) {
            ypush_long(tao_remote_camera_get_nbufs(cam));
            return;
        }
        if (match("ncmds", 5)) {
            ypush_long(tao_remote_camera_get_ncmds(cam));
            return;
        }
        if (match("nrefs", 5)) {
            ypush_long(cam->base.base.nrefs);
            return;
        }
        break;
    case 'o':
        if (match("owner", 5)) {
            ytao_push_string(tao_remote_camera_get_owner(cam));
            return;
        }
        if (match("overflows", 9)) {
            ypush_long(cam->config.overflows);
            return;
        }
        if (match("overruns", 8)) {
            ypush_long(cam->config.overruns);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            ypush_long(tao_remote_camera_get_flags(cam) & 0x1ff);
            return;
        }
        if (match("pid", 3)) {
            ypush_long(tao_remote_camera_get_pid(cam));
            return;
        }
        if (match("pixeltype", 9)) {
            ypush_long(tao_remote_camera_get_pixeltype(cam));
            return;
        }
        if (match("preprocessing", 13)) {
            ypush_long(tao_remote_camera_get_preprocessing(cam));
            return;
        }
        if (match("preprocshmids", 13)) {
            long dims[2] = {1, 4};
            long* shmid = ypush_l(dims);
            for (int i = 0; i < 4; ++i) {
                shmid[i] = tao_remote_camera_get_preprocessing_shmid(cam, i);
            }
            return;
        }
        break;
    case 'r':
        if (match("rawencoding", 11)) {
            ypush_long(tao_remote_camera_get_rawencoding(cam));
            return;
        }
        if (match("roi", 3)) {
            long dims[2] = {1, 4};
            long* roi = ypush_l(dims);
            roi[0] = tao_remote_camera_get_xoff(cam);
            roi[1] = tao_remote_camera_get_yoff(cam);
            roi[2] = tao_remote_camera_get_width(cam);
            roi[3] = tao_remote_camera_get_height(cam);
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(tao_remote_camera_get_serial(cam));
            return;
        }
        if (match("state", 5)) {
            ypush_int(tao_remote_camera_get_state(cam));
            return;
        }
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        if (match("sensorheight", 12)) {
            ypush_long(tao_remote_camera_get_sensorheight(cam));
            return;
        }
        if (match("sensorwidth", 11)) {
            ypush_long(tao_remote_camera_get_sensorwidth(cam));
            return;
        }
        break;
    case 't':
#if 0 // FIXME:
        if (match("temperature", 11)) {
            ypush_double(cam->config.temperature);
            return;
        }
#endif
        if (match("timeouts", 8)) {
            ypush_long(cam->config.timeouts);
            return;
        }
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    case 'w':
        if (match("width", 5)) {
            ypush_long(tao_remote_camera_get_width(cam));
            return;
        }
        break;
    case 'x':
        if (match("xbin", 4)) {
            ypush_long(tao_remote_camera_get_xbin(cam));
            return;
        }
        if (match("xoff", 4)) {
            ypush_long(tao_remote_camera_get_xoff(cam));
            return;
        }
        break;
    case 'y':
        if (match("ybin", 4)) {
            ypush_long(tao_remote_camera_get_ybin(cam));
            return;
        }
        if (match("yoff", 4)) {
            ypush_long(tao_remote_camera_get_yoff(cam));
            return;
        }
        break;
    }
    // Maybe a named attribute.
    long j = tao_remote_camera_try_find_attr(cam, name);
    if (j < 0) {
        ytao_throw("bad member `%s`", name);
    }
    const tao_attr* attr = tao_remote_camera_unsafe_get_attr(cam, j);
    if (attr == NULL) {
        y_error("unexpected NULL attribute pointer");
    }
    switch (TAO_ATTR_TYPE(attr)) {
    case TAO_ATTR_BOOLEAN:
        ypush_int(attr->val.b ? 1 : 0);
        break;
    case TAO_ATTR_INTEGER:
        ypush_long(attr->val.i);
        break;
    case TAO_ATTR_FLOAT:
        ypush_double(attr->val.f);
        break;
    case TAO_ATTR_STRING:
        ytao_push_string(attr->val.s);
        break;
    default:
        ytao_throw("unknown type of value for named attribute `%s`", name);
    }
}

static void remote_camera_eval(ytao_object* yobj, int argc)
{
    if (argc != 1) {
        y_error("expecting exactly one, possibly void, argument");
    }
    int type = yarg_typeid(0);
    if (type == Y_VOID) {
        // Yield the list of named attributes.
        tao_remote_camera* cam = yobj->shared;
        long len = tao_remote_camera_get_attr_number(cam);
        if (len == 0) {
            ypush_nil();
        } else {
            long dims[2] = {1, len};
            ystring_t* arr = ypush_q(dims);
            for (long j = 0; j < len; ++j) {
                const tao_attr* attr = tao_remote_camera_unsafe_get_attr(cam, j);
                arr[j] = p_strcpy(tao_attr_get_key(attr));
            }
        }
    } else if (type == Y_STRING) {
        const char* key = ygets_q(0);
        remote_camera_extract(yobj, key);
    } else {
        y_error("argument must be nil or a scalar string");
    }
}

ytao_operations ytao_remote_camera_ops = {
    // TAO_REMOTE_CAMERA,
    false,
    remote_camera_eval,
    remote_camera_extract,
    NULL, // on_wait_data_frame
};
