// macros.h -
//
// Common macros for Yorick interface to TAO real-time software.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#ifndef YTAO_MACROS_H_
#define YTAO_MACROS_H_ 1

typedef float  float32_t;
typedef double float64_t;

// Macro used in *_extract callbacks.
#define match(s,n) (len == (n) && memcmp(name, (s), (n)) == 0)

// Simple range values returned by yget_range when start and stop are not
// given.
#define RANGE_COLON  (1|Y_MIN_DFLT|Y_MAX_DFLT)
#define RANGE_STAR   (Y_RUBBER1|Y_MIN_DFLT|Y_MAX_DFLT)
#define RANGE_PLUS   (Y_MMMARK|Y_MIN_DFLT|Y_MAX_DFLT)
#define RANGE_MINUS  (Y_PSEUDO|Y_MIN_DFLT|Y_MAX_DFLT)
#define RANGE_RUBBER (Y_RUBBER|Y_MIN_DFLT|Y_MAX_DFLT)

#define MAX_SECONDS ((double)TAO_MAX_TIME_SECONDS)
#define WAIT_FOREVER (2.0*MAX_SECONDS)

// Yields the length of a static array.
#define LENGTH(arr) (sizeof(arr)/sizeof(arr[0]))

#define SIZE_OF_MEMBER(T,m) sizeof(((T*)0)->m)

// Yields whether numerical type T is signed
#define IS_SIGNED(T) ((T)(-1) < (T)(0))

// Maximum number of array dimensions.
#define MAX_NDIMS TAO_MIN(TAO_MAX_NDIMS, Y_DIMSIZE - 1)

#endif // YTAO_MACROS_H_ ------------------------------------------------------
