// ytao_shared_object.c -
//
// Yorick interface to TAO shared objects.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-shared-objects-private.h>

// For the `on_extract` callback, there are no needs to lock a shared object if
// we already have a reference on it (which is warranted if it is attached to
// our address space) and if we only read immutable information.  Using string
// comparison is fast enough (worst case here takes 75ns, best case takes
// 58ns).

static void shared_object_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    switch (name[0]) {
    case 'f':
        if (match("flags", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->flags);
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        break;
    case 'n':
        if (match("nrefs", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->nrefs);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->flags & 0x1ff);
            return;
        }
        break;
    case 's':
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        break;
    case 't':
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    }
    y_error("bad member");
}

static void shared_object_eval(ytao_object* yobj, int argc)
{
    y_error("the obj(...) syntax is not implemented for this kind of object");
}

ytao_operations ytao_shared_object_ops = {
    // TAO_SHARED_OBJECT,
    false,
    shared_object_eval,
    shared_object_extract
};
