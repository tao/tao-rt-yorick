// ytao.c -
//
// Implements Yorick interface to TAO real-time software.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-config.h>

// We need private definitions.
#include <tao-remote-cameras-private.h>
#include <tao-remote-mirrors-private.h>
#include <tao-remote-objects-private.h>
#include <tao-remote-sensors-private.h>
#include <tao-rwlocked-objects-private.h>
#include <tao-rwlocked-objects-private.h>
#include <tao-shared-arrays-private.h>
#include <tao-shared-objects-private.h>

// Index of possible keywords in gloabl table of names.  Initialized at
// startup.
static long rawencoding_index = -1L;
static long buffers_index = -1L;
static long exposuretime_index = -1L;
static long framerate_index = -1L;
static long height_index = -1L;
static long perms_index = -1L;
static long pixeltype_index = -1L;
static long preprocessing_index = -1L;
static long width_index = -1L;
static long xbin_index = -1L;
static long xoff_index = -1L;
static long ybin_index = -1L;
static long yoff_index = -1L;

// Array indexed by TAO/Yorick array element type to convert tp Yorick/TAO
// array element type.
static int to_yorick_eltype_[TAO_DOUBLE - TAO_INT8 + 1];
static int to_tao_eltype_[Y_DOUBLE - Y_CHAR + 1];
int const* ytao_to_yorick_eltype_ = NULL;
int const* ytao_to_tao_eltype_ = NULL;

// Private resources.
tao_time ytao_monotonic_origin = TAO_UNKNOWN_TIME;
tao_buffer ytao_dynbuf = TAO_BUFFER_INITIALIZER;
static bool debug = true;

//-----------------------------------------------------------------------------
// SHARED OBJECTS

static const char* lock_name(ytao_lock_state lock)
{
    switch (lock) {
    case YTAO_LOCK_READONLY:  return "read-only";
    case YTAO_LOCK_READWRITE: return "read-write";
    default:                  return "none";
    }
}

// Buffer `buf` must have at least 32 bytes.
static void get_object_type_name(char* buf, uint32_t type)
{
    switch (type) {
    case TAO_SHARED_OBJECT:
        strcpy(buf, "TAO_SHARED_OBJECT");
        break;
    case TAO_RWLOCKED_OBJECT:
        strcpy(buf, "TAO_RWLOCKED_OBJECT");
        break;
    case TAO_REMOTE_OBJECT:
        strcpy(buf, "TAO_REMOTE_OBJECT");
        break;
    case TAO_SHARED_ARRAY:
        strcpy(buf, "TAO_SHARED_ARRAY");
        break;
    case TAO_REMOTE_CAMERA:
        strcpy(buf, "TAO_REMOTE_CAMERA");
        break;
    case TAO_REMOTE_MIRROR:
        strcpy(buf, "TAO_REMOTE_MIRROR");
        break;
    case TAO_REMOTE_SENSOR:
        strcpy(buf, "TAO_REMOTE_SENSOR");
        break;
    default:
        sprintf(buf, "0x%08x", (unsigned int)type);
    }
}

static void ytao_object_free(void* addr)
{
    ytao_object* yobj = addr;
    if (yobj->shared != NULL) {
        tao_shared_object* obj = yobj->shared;
        if (yobj->lock != YTAO_LOCK_NONE) {
            if (yobj->ops != NULL) {
                if (yobj->ops->rwlocked) {
                    tao_rwlocked_object_unlock((tao_rwlocked_object*)obj);
                } else {
                    tao_shared_object_unlock(obj);
                }
            }
            yobj->lock = YTAO_LOCK_NONE;
        }
        yobj->shared = NULL;
        tao_shared_object_detach(obj);
    }
    if (yobj->private != NULL) {
        ydrop_use(yobj->private);
    }
}

static void ytao_object_print(void* addr)
{
    ytao_object* yobj = addr;
    char type_name[32];
    tao_buffer_clear(&ytao_dynbuf);
    get_object_type_name(type_name, yobj->type);
    if (tao_buffer_printf(
            &ytao_dynbuf,
            "TAO shared object (shmid=%ld, type=%s, size=%ld, lock=%s",
            (long)yobj->shmid, type_name, (long)yobj->size,
            lock_name(yobj->lock)) != TAO_OK) {
        ytao_throw_last_error();
    }
    y_print(tao_buffer_get_contents(&ytao_dynbuf, NULL), 0);
    tao_buffer_clear(&ytao_dynbuf);
    if (yobj->owner[0] != '\0') {
        y_print(", owner=\"", 0);
        y_print(yobj->owner, 0);
        y_print("\")", 1);
    } else {
        y_print(")", 1);
    }
}

static void ytao_object_eval(void* addr, int argc)
{
    ytao_object* yobj = addr;
    yobj->ops->on_eval(yobj, argc);
}

static void ytao_object_extract(void* addr, char* name)
{
    ytao_object* yobj = addr;
    yobj->ops->on_extract(yobj, name);
}

y_userobj_t ytao_object_type = {
    "tao_shared_object",
    ytao_object_free,
    ytao_object_print,
    ytao_object_eval,
    ytao_object_extract,
    NULL
};

// To avoid memory leaks in case creating the Yorick object fails, attaching to
// a shared object must be done is a 3 steps operation: (1) push Yorick object
// on top of stack; (2) attach to shared object; (3) initialize Yorick object
// members.  Step 1 is done by the macro `ytao_push_new_object`.  Steps 2 is
// done by the caller.  Step 3 is performed by the function
// `ytao_initialize_object`.

#define ytao_push_new_object(_) \
    ((ytao_object*)ypush_obj(&ytao_object_type, sizeof(ytao_object)))

static inline void ytao_initialize_object(
    ytao_object* yobj,
    const ytao_operations* ops)
{
    tao_shared_object* shared = yobj->shared;
    if (shared == NULL) {
        ytao_throw_last_error();
    }
    yobj->ops   = ops;
    yobj->shmid = tao_shared_object_get_shmid(shared);
    yobj->size  = tao_shared_object_get_size(shared);
    yobj->type  = tao_shared_object_get_type(shared);
    yobj->lock  = YTAO_LOCK_NONE;
    if (ops == &ytao_remote_mirror_ops) {
        yobj->owner = tao_remote_mirror_get_owner(
            (tao_remote_mirror*)shared);
    } else if (ops == &ytao_remote_object_ops) {
        yobj->owner = tao_remote_object_get_owner(
            (tao_remote_object*)shared);
     } else if (ops == &ytao_remote_camera_ops) {
        yobj->owner = tao_remote_camera_get_owner(
            (tao_remote_camera*)shared);
    } else {
        yobj->owner = ""; // FIXME:
    }
}

//-----------------------------------------------------------------------------
// PARSING OF ARGUMENTS

static double get_optional_timeout(int iarg)
{
    if (iarg < 0 || yarg_nil(iarg)) {
        return WAIT_FOREVER;
    } else {
        return ygets_d(iarg);
    }
}

#if 0 // FIXME: unused
static double get_timeout(int iarg)
{
    if (iarg < 0) y_error("missing number of seconds");
    return ygets_d(iarg);
}
#endif

static tao_serial get_serial_number(int iarg)
{
    if (iarg < 0) y_error("missing serial number");
    return ygets_l(iarg);
}

static tao_shmid get_shmid(int iarg, bool check)
{
    if (yarg_rank(iarg) == 0) {
        // Got a scalar.
        int arg_type = yarg_typeid(iarg);
        if (Y_CHAR <= arg_type && arg_type <= Y_LONG) {
            // Got an integer.
            tao_shmid shmid = ygets_l(iarg);
            if (shmid < 0) {
                if (check) y_error("invalid shared memory identifier");
                shmid = TAO_BAD_SHMID;
            }
            return shmid;
        } else if (arg_type == Y_STRING) {
            const char* owner = ygets_q(iarg);
            tao_shmid shmid = tao_config_read_shmid(owner);
            if (shmid < 0) {
                if (check) y_error("unreachable remote server");
                shmid = TAO_BAD_SHMID;
            }
             return shmid;
        }
    }
    y_error("expecting an integer or a remote server name");
}

static tao_shmid get_remote_shmid(int iarg, bool check)
{
    if (yarg_rank(iarg) == 0) {
        // Got a scalar.
        int arg_type = yarg_typeid(iarg);
        tao_shmid shmid;
        if (arg_type == Y_STRING) {
            // Got a string.
            shmid = tao_config_read_shmid(ygets_q(iarg));
            if (shmid < 0) {
                if (check) y_error("invalid server name");
                shmid = TAO_BAD_SHMID;
            }
            return shmid;
        } else if (Y_CHAR <= arg_type && arg_type <= Y_LONG) {
            // Got an integer.
            shmid = ygets_l(iarg);
            if (shmid < 0) {
                if (check) y_error("invalid shared memory identifier");
                shmid = TAO_BAD_SHMID;
            }
            return shmid;
        }
    }
    y_error("expecting an integer or a server name");
}

// For an argument of type Y_STRUCTDEF, yields one of Y_CHAR, Y_SHORT, Y_INT,
// Y_LONG, etc.; for other arguments, returns -1.
static int get_structdef_type(
    int iarg)
{
    Symbol* stack = sp - iarg;
    if (stack->ops != NULL) {
        Operand op;
        stack->ops->FormOperand(stack, &op);
        if (op.ops == &structDefOps) {
            StructDef* base = op.value;
            if (base == &charStruct) {
                return Y_CHAR;
            } else if (base == &shortStruct) {
                return Y_SHORT;
            } else if (base == &intStruct) {
                return Y_INT;
            } else if (base == &longStruct) {
                return Y_LONG;
            } else if (base == &floatStruct) {
                return Y_FLOAT;
            } else if (base == &doubleStruct) {
                return Y_DOUBLE;
            } else if (base == &complexStruct) {
                return Y_COMPLEX;
            } else if (base == &stringStruct) {
                return Y_STRING;
            } else if (base == &pointerStruct) {
                return Y_POINTER;
            }
        }
    }
    return -1;
}

// Yields TAO array element type (TAO_UINT8, TAO_UINT16, etc.).
static tao_eltype get_pixeltype(
    int iarg)
{
    int typeid = yarg_typeid(iarg);
    if (typeid == Y_CHAR || typeid == Y_SHORT ||
        typeid == Y_INT || typeid == Y_LONG) {
        if (yarg_rank(iarg) == 0) {
            long val = ygets_l(iarg);
            if (val >= TAO_INT8 && val <= TAO_DOUBLE) {
                return val;
            }
        }
    } else if (typeid == Y_STRUCTDEF) {
        int type = get_structdef_type(iarg);
        int size = 0;
        if (type == Y_CHAR) {
            size = sizeof(char);
        } else if (type == Y_SHORT) {
            size = sizeof(short);
        } else if (type == Y_INT) {
            size = sizeof(int);
        } else if (type == Y_LONG) {
            size = sizeof(long);
        } else if (type == Y_FLOAT) {
            return TAO_FLOAT;
        } else if (type == Y_DOUBLE) {
            return TAO_DOUBLE;
        }
        if (size == 1) {
            return TAO_UINT8;
        } else if (size == 2) {
            return TAO_UINT16;
        } else if (size == 4) {
            return TAO_UINT32;
        } else if (size == 8) {
            return TAO_UINT64;
        }
    }
    y_error("unsupported pixel type");
}

// Yields TAO encoding.
static tao_encoding get_encoding(
    int iarg)
{
    if (yarg_rank(iarg) == 0) {
        // We've got a scalar.
        int typeid = yarg_typeid(iarg);
        if (typeid == Y_CHAR || typeid == Y_SHORT ||
            typeid == Y_INT || typeid == Y_LONG) {
            return (tao_encoding)ygets_l(iarg);
        }
        if (typeid == Y_STRING) {
            tao_encoding enc = tao_parse_encoding(ygets_q(iarg));
            if (enc == TAO_ENCODING_UNKNOWN) {
                y_error("invalid encoding name");
            }
            return enc;
        }
    }
    y_error("encoding must be specified as an integer or as a string");
}

static const char* format_preprocessing(
    tao_preprocessing proc)
{
    switch (proc) {
    case TAO_PREPROCESSING_NONE:
        return "none";
    case TAO_PREPROCESSING_AFFINE:
        return "affine";
    case TAO_PREPROCESSING_FULL:
        return "full";
    default:
        y_error("invalid pre-processing value");
    }
}

static tao_preprocessing parse_preprocessing(
    const char* str)
{
    if (str != NULL) {
        if (strcmp(str, "none") == 0) {
            return TAO_PREPROCESSING_NONE;
        } else if (strcmp(str, "affine") == 0) {
            return TAO_PREPROCESSING_AFFINE;
        } else if (strcmp(str, "full") == 0) {
            return TAO_PREPROCESSING_FULL;
        }
    }
    y_error("invalid pre-processing name");
}

// Yields TAO preprocessing.
static tao_preprocessing get_preprocessing(
    int iarg)
{
    if (yarg_rank(iarg) == 0) {
        // We've got a scalar.
        int typeid = yarg_typeid(iarg);
        if (typeid == Y_CHAR || typeid == Y_SHORT ||
            typeid == Y_INT || typeid == Y_LONG) {
            tao_preprocessing val = (tao_preprocessing)ygets_l(iarg);
            switch (val) {
            case TAO_PREPROCESSING_NONE:
            case TAO_PREPROCESSING_AFFINE:
            case TAO_PREPROCESSING_FULL:
                return val;
            default:
                y_error("invalid pre-processing value");
            }
        }
        if (typeid == Y_STRING) {
            return parse_preprocessing(ygets_q(iarg));
        }
    }
    y_error("pre-processing must be specified as an integer or as a string");
}

//-----------------------------------------------------------------------------
// BUILT-IN FUNCTIONS

void Y_tao_library_version(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    int major, minor, patch;
    tao_version_get(&major, &minor, &patch);
    if (yarg_nil(0)) {
        long* vals = ypush_l((long[]){1,3});
        vals[0] = major;
        vals[1] = minor;
        vals[2] = patch;
        return;
    }
    if (yarg_rank(0) == 0) {
        // Got a scalar.
        int type = yarg_typeid(0);
        if (type == Y_CHAR || type == Y_SHORT || type == Y_INT || type == Y_LONG) {
            long idx = ygets_l(0);
            if (idx <= 0) {
                idx += 3;
            }
            long val = -1;
            if (idx == 1) {
                val = major;
            } else if (idx == 2) {
                val = minor;
            } else if (idx == 3) {
                val = patch;
            } else {
                y_error("out of range version part index");
            }
            ypush_long(val);
            return;
        }
        if (type == Y_STRING) {
            const char* key = ygets_q(0);
            long val = -1;
            if (key != NULL) {
                if (strcmp(key, "major") == 0) {
                    val = major;
                } else if (strcmp(key, "minor") == 0) {
                    val = minor;
                } else if (strcmp(key, "patch") == 0) {
                    val = patch;
                } else {
                    y_error("unknown version part key");
                }
                ypush_long(val);
                return;
            }
        }
    }
    y_error("argument must be nil, integer, or string");
}

void Y_tao_config_read_shmid(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    ypush_long(tao_config_read_shmid(ygets_q(argc - 1)));
}

void Y_tao_attach(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_shmid(argc - 1, true);
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_shared_object_attach(shmid);
    tao_object_type type = tao_shared_object_get_type(yobj->shared);
    ytao_operations* ops = NULL;
    if (type == TAO_SHARED_ARRAY) {
        ops = &ytao_shared_array_ops;
    } else if (type == TAO_REMOTE_CAMERA) {
        ops = &ytao_remote_camera_ops;
    } else if (type == TAO_REMOTE_MIRROR) {
        ops = &ytao_remote_mirror_ops;
    } else if ((type & TAO_SHARED_SUPERTYPE_MASK) == TAO_REMOTE_OBJECT) {
        ops = &ytao_remote_object_ops;
    } else if ((type & TAO_SHARED_SUPERTYPE_MASK) == TAO_RWLOCKED_OBJECT) {
        ops = &ytao_rwlocked_object_ops;
    } else {
        ops = &ytao_shared_object_ops;
    }
    ytao_initialize_object(yobj, ops);
}

void Y_tao_attach_shared_object(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_shmid(argc - 1, true);
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_shared_object_attach(shmid);
    ytao_initialize_object(yobj, &ytao_shared_object_ops);
}

void Y_tao_attach_shared_array(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_shmid(argc - 1, true);
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_shared_array_attach(shmid);
    ytao_initialize_object(yobj, &ytao_shared_array_ops);
}

void Y_tao_attach_remote_camera(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_shmid(argc - 1, true);
    ytao_object*yobj = ytao_push_new_object();
    yobj->shared = tao_remote_camera_attach(shmid);
    ytao_initialize_object(yobj, &ytao_remote_camera_ops);
}

void Y_tao_attach_remote_mirror(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_remote_shmid(argc - 1, true);
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_remote_mirror_attach(shmid);
    ytao_initialize_object(yobj, &ytao_remote_mirror_ops);
}

void Y_tao_attach_remote_sensor(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_shmid shmid = get_remote_shmid(argc - 1, true);
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_remote_sensor_attach(shmid);
    ytao_initialize_object(yobj, &ytao_remote_sensor_ops);
}

void Y_tao_create_shared_object(int argc)
{
    if (argc < 1 || argc > 3) y_error("expecting 1 to 3 arguments");
    int type = ygets_l(--argc);
    long size = (--argc >= 0 ? ygets_l(argc) : sizeof(tao_shared_object));
    unsigned int flags = (--argc >= 0 ? ygets_l(argc) : 0);
    if (type >= 0 && type < 31) type |= TAO_SHARED_OBJECT;
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_SHARED_OBJECT) {
        y_error("incorrect type for a shared object");
    }
    if (size <  sizeof(tao_shared_object)) y_error("invalid size");
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_shared_object_create(type, size, flags);
    ytao_initialize_object(yobj, &ytao_shared_object_ops);
}

void Y_tao_create_remote_object(int argc)
{
    if (argc < 5 || argc > 7) y_error("expecting 5 to 7 arguments");
    const char* owner = ygets_q(--argc);
    int type          = ygets_l(--argc);
    long nbufs        = ygets_l(--argc);
    long offset       = ygets_l(--argc);
    long stride       = ygets_l(--argc);
    long size         = (--argc >= 0 ? ygets_l(argc) : offset + nbufs*stride);
    unsigned flags    = (--argc >= 0 ? ygets_l(argc) : 0);
    size_t len = TAO_STRLEN(owner);
    if (len < 1) y_error("owner name is too short");
    if (len >= TAO_OWNER_SIZE) y_error("owner name is too long");
    if (type >= 0 && type < 32) type |= TAO_REMOTE_OBJECT;
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_REMOTE_OBJECT) {
        y_error("incorrect type for a remote object");
    }
    if (size <  offset + nbufs*stride) y_error("invalid size");
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_remote_object_create(
        owner, type, nbufs, offset, stride, size, flags);
    ytao_initialize_object(yobj, &ytao_remote_object_ops);
}

void Y_tao_create_rwlocked_object(int argc)
{
    if (argc < 1 || argc > 3) y_error("expecting 1 to 3 arguments");
    long min_size  = sizeof(tao_rwlocked_object);
    int type       = ygets_l(--argc);
    long size      = (--argc >= 0 ? ygets_l(argc) : min_size);
    unsigned flags = (--argc >= 0 ? ygets_l(argc) : 0);
    if (type >= 0 && type < 32) type |= TAO_RWLOCKED_OBJECT;
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_RWLOCKED_OBJECT) {
        y_error("incorrect type for a rwlocked object");
    }
    if (size < min_size) y_error("invalid size");
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_rwlocked_object_create(type, size, flags);
    ytao_initialize_object(yobj, &ytao_rwlocked_object_ops);
}

static void grow_dims(int iarg, long dims[], long max_ndims)
{
    long ndims = dims[0];
    int type = yarg_typeid(iarg);
    if (max_ndims == -1) {
        max_ndims = Y_DIMSIZE - 1;
    }
    if (type <= Y_LONG) {
        int rank = yarg_rank(iarg);
        if (rank == 0) {
            long dim = ygets_l(iarg);
            if (dim < 1) {
                goto bad_dim;
            }
            if (ndims + 1 > max_ndims) {
                goto too_many_dims;
            }
            ++ndims;
            dims[0] = ndims;
            dims[ndims] = dim;
            return;
        } else if (rank == 1) {
            long i, ntot;
            long* vals = ygeta_l(iarg, &ntot, NULL);
            if (ntot < 1 || vals[0] != ntot - 1) {
                goto bad_dimlist;
            }
            if (ndims + ntot - 1 > max_ndims) {
                goto too_many_dims;
            }
            for (i = 1; i < ntot; ++i) {
                long dim = vals[i];
                if (dim < 1) {
                    goto bad_dim;
                }
                ++ndims;
                dims[0] = ndims;
                dims[ndims] = dim;
            }
            return;
        }
    } else if (type == Y_VOID) {
        return;
    }
 bad_dimlist:
    y_error("invalid dimension list");
 bad_dim:
    y_error("invalid dimension");
 too_many_dims:
    y_error("too many dimensions");
}

void Y_tao_create_shared_array(int argc)
{
    // Parse positional arguments and other keywords.
    long dims[Y_DIMSIZE];
    int type = -1;
    unsigned int perms = 0;
    dims[0] = 0;
    for (int iarg = argc - 1; iarg >= 0; --iarg) {
        long index = yarg_key(iarg);
        if (index < 0) {
            // Positional argument.
            if (type == -1) {
                // get type or value argument
                type = get_structdef_type(iarg);
                if (type == -1) {
                    y_error("first argument to tao_shared_array_create() "
                            "must be a type");
                }
                if (!YTAO_CHECK_YORICK_ELTYPE(type)) {
                    y_error("unsupported element type");
                }
                type = YTAO_TO_TAO_ELTYPE(type);
            } else {
                grow_dims(iarg, dims, MAX_NDIMS);
            }
        } else {
            // Keyword argument.
            --iarg;
            if (index == perms_index) {
                perms = ygets_l(iarg);
            } else {
                y_error("unsupported keyword");
            }
        }
    }
    if (type < 0) {
        y_error("too few arguments to tao_shared_array_create() function");
    }

    // Push new shared array on top of the stack.
    ytao_object* yobj = ytao_push_new_object();
    yobj->shared = tao_shared_array_create(
        type, dims[0], &dims[1], perms);
    ytao_initialize_object(yobj, &ytao_shared_array_ops);
}

void Y_tao_is_shared_object(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    if (yget_obj(0, NULL) != ytao_object_type.type_name) {
        ypush_long(0);
    } else {
        ypush_long(ytao_get_object(0)->type);
    }
}

void Y_tao_get_state_name(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    tao_state state = TAO_STATE_UNREACHABLE;
    int type = yarg_typeid(0);
    if (type >= Y_CHAR && type <= Y_LONG && yarg_rank(0) == 0) {
        // Got a scalar integer.
        state = ygets_l(0);
    } else if (yget_obj(0, NULL) == ytao_object_type.type_name) {
         ytao_object* yobj = ytao_get_object(0);
         if (yobj->ops == &ytao_remote_mirror_ops) {
             state = tao_remote_mirror_get_state(
                 (tao_remote_mirror*)yobj->shared);
         } else if (yobj->ops == &ytao_remote_camera_ops) {
             state = tao_remote_camera_get_state(
                 (tao_remote_camera*)yobj->shared);
         } else {
             y_error("invalid TAO shared object");
         }
    } else {
        y_error("expecting an integer or a TAO shared object");
    }
    ytao_push_string(tao_state_get_name(state));
}

void Y_tao_is_locked(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    if (yget_obj(0, NULL) != ytao_object_type.type_name) {
        ypush_int(0);
    } else {
        ypush_int(ytao_get_object(0)->lock);
    }
}

// Do all the work before locking an object.
static inline ytao_object* parse_lock_object_builtin(
    int argc,
    double* secs)
{
    // Retrieve arguments.
    if (argc < 1 || argc > 2) {
        y_error("expecting one or 2 arguments");
    }
    int iarg = argc - 1;
    ytao_object* yobj = ytao_get_object(iarg);
    if (--iarg >= 0 && ! yarg_nil(iarg)) {
        *secs = ygets_d(iarg);
    } else {
        // Manage to wait forever.
        *secs = WAIT_FOREVER;
    }

    // Check that there are no pending signals before returning the object.
    if (p_signalling) {
        p_abort();
    }
    return yobj;
}

void Y_tao_lock(int argc)
{
    double secs;
    ytao_object* yobj = parse_lock_object_builtin(argc, &secs);
    if (yobj->ops->rwlocked) {
        y_error("exclusive lock is not implemented for this type of object");
    }
    if (yobj->lock != YTAO_LOCK_NONE) {
        y_error("object is already locked by caller");
    }
    tao_status status;
    tao_shared_object* obj = yobj->shared;
    if (secs > MAX_SECONDS) {
        status = tao_shared_object_lock(obj);
    } else if (secs == 0) {
        status = tao_shared_object_try_lock(obj);
    } else {
        status = tao_shared_object_timed_lock(obj, secs);
    }
    if (status == TAO_OK) {
        yobj->lock = YTAO_LOCK_READWRITE;
    } else if (status != TAO_TIMEOUT) {
        ytao_throw_last_error();
    }
    ypush_int(yobj->lock);
}

void Y_tao_rdlock(int argc)
{
    double secs;
    ytao_object* yobj = parse_lock_object_builtin(argc, &secs);
    if (!yobj->ops->rwlocked) {
        y_error("read-only lock is not implemented for this type of object");
    }
    if (yobj->lock != YTAO_LOCK_NONE) {
        y_error("object is already locked by caller");
    }
    tao_status status;
    tao_rwlocked_object* obj = yobj->shared;
    if (secs > MAX_SECONDS) {
        status = tao_rwlocked_object_rdlock(obj);
    } else if (secs == 0) {
        status = tao_rwlocked_object_try_rdlock(obj);
    } else {
        status = tao_rwlocked_object_timed_rdlock(obj, secs);
    }
    if (status == TAO_OK) {
        yobj->lock = YTAO_LOCK_READONLY;
    } else if (status != TAO_TIMEOUT) {
        ytao_throw_last_error();
    }
    ypush_int(yobj->lock);
}

void Y_tao_wrlock(int argc)
{
    double secs;
    ytao_object* yobj = parse_lock_object_builtin(argc, &secs);
    if (!yobj->ops->rwlocked) {
        y_error("read-write lock is not implemented for this type of object");
    }
    if (yobj->lock != YTAO_LOCK_NONE) {
        y_error("object is already locked by caller");
    }
    tao_status status;
    tao_rwlocked_object* obj = yobj->shared;
    if (secs > MAX_SECONDS) {
        status = tao_rwlocked_object_wrlock(obj);
    } else if (secs == 0) {
        status = tao_rwlocked_object_try_wrlock(obj);
    } else {
        status = tao_rwlocked_object_timed_wrlock(obj, secs);
    }
    if (status == TAO_OK) {
        yobj->lock = YTAO_LOCK_READWRITE;
    } else if (status != TAO_TIMEOUT) {
        ytao_throw_last_error();
    }
    ypush_int(yobj->lock);
}

// Unlock an object that is currently locked by the caller.
static void force_unlock(ytao_object* yobj)
{
    tao_status status;
    if (yobj->ops->rwlocked) {
        tao_rwlocked_object* obj = yobj->shared;
        status = tao_rwlocked_object_unlock(obj);
    } else {
        tao_shared_object* obj = yobj->shared;
        status = tao_shared_object_unlock(obj);
    }
    if (status != TAO_OK) {
        ytao_throw_last_error();
    }
    yobj->lock = YTAO_LOCK_NONE;
}

void Y_tao_unlock(int argc)
{
    // Check arguments and unlock object.
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    ytao_object* yobj = ytao_get_object(0);
    if (yobj->lock == YTAO_LOCK_NONE) {
        y_error("object is not locked by caller");
    }
    force_unlock(yobj);
    ypush_nil();
}

void Y_tao_ensure_unlocked(int argc)
{
    int iarg;
    for (iarg = argc - 1; iarg >= 0; --iarg) {
        // Unlock if argument is a locked shared object.
        if (yget_obj(iarg, NULL) == ytao_object_type.type_name) {
            ytao_object* yobj = ytao_get_object(iarg);
            if (yobj->lock != YTAO_LOCK_NONE) {
                force_unlock(yobj);
            }
        }
    }
    ypush_nil();
}

// Calling a built-in function is slightly faster than extracting an object
// member:
//
//  `tao_get_serial(arr)` ------> 32 ns     `arr.serial` ------> 56 ns
//  `tao_get_timestamp(arr)` ---> 76 ns     `arr.timestamp` ---> 106 ns
//
// So it seems that there is an overhead of ~ 30 ns when extracting an object
// member.

void Y_tao_get_serial(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    ytao_object* yobj = ytao_get_object(0);
    tao_serial serial;
    if (yobj->ops == &ytao_shared_array_ops) {
        serial = tao_shared_array_get_serial(
            (tao_shared_array*)yobj->shared);
    } else if (yobj->ops == &ytao_remote_camera_ops) {
        serial = tao_remote_camera_get_serial(
            (tao_remote_camera*)yobj->shared);
    } else if (yobj->ops == &ytao_remote_mirror_ops) {
        serial = tao_remote_mirror_get_serial(
            (tao_remote_mirror*)yobj->shared);
    } else {
        y_error("invalid TAO shared object");
        return;
    }
    ypush_long(serial);
}

void Y_tao_set_serial(int argc)
{
    if (argc != 2) {
        y_error("expecting exactly 2 arguments");
    }
    tao_shared_array_set_serial(ytao_get_shared_array(argc - 1),
                                 ygets_l(argc - 2));
    yarg_drop(argc - 1);
}

static inline int get_array_timestamp_index(int iarg)
{
    long idx = ygets_l(iarg);
    if (idx <= 0) {
        idx += TAO_SHARED_ARRAY_TIMESTAMPS;
    }
    if (idx < 1 || idx > TAO_SHARED_ARRAY_TIMESTAMPS) {
        y_error("out of range time-stamp index");
    }
    return idx - 1;
}

void Y_tao_get_timestamp(int argc)
{
    if (argc != 2) {
        y_error("expecting exactly 2 arguments");
    }
    ytao_push_shared_array_timestamp(ytao_get_shared_array(argc - 1),
                                     get_array_timestamp_index(0));
}

void Y_tao_set_timestamp(int argc)
{
    if (argc != 3) {
        y_error("expecting exactly 3 arguments");
    }

    // Parse the time-stamp as either a (fractional) number of seconds or
    // as two integers (the number of seconds and of nanoseconds).
    int iarg = argc;
    tao_shared_array* arr = ytao_get_shared_array(--iarg);
    int idx = get_array_timestamp_index(--iarg);
    bool success = false;
    tao_time t = TAO_UNKNOWN_TIME;
    int rank = yarg_rank(--iarg);
    if (rank == 0) {
        double a = ygets_d(iarg);
        double b = floor(a);
        t.sec = ytao_monotonic_origin.sec + (int64_t)b;
        t.nsec = ytao_monotonic_origin.nsec
            + lround((a - b)*TAO_NANOSECONDS_PER_SECOND);
        success = true;
    } else if (rank == 1) {
        long dims[Y_DIMSIZE];
        int type;
        long ntot;
        void* ptr = ygeta_any(iarg, &ntot, dims, &type);
        if (dims[0] == 1 && ntot == 2) {
            if (type == Y_LONG) {
                t.sec = ((long*)ptr)[0];
                t.nsec = ((long*)ptr)[1];
                success = true;
            } else if (type == Y_INT) {
                t.sec = ((int*)ptr)[0];
                t.nsec = ((int*)ptr)[1];
                success = true;
            } else if (type == Y_SHORT) {
                t.sec = ((short*)ptr)[0];
                t.nsec = ((short*)ptr)[1];
                success = true;
            } else if (type == Y_CHAR) {
                t.sec = ((char*)ptr)[0];
                t.nsec = ((char*)ptr)[1];
                success = true;
            }
        }
    }
    if (! success) {
        y_error("time-stamp is `sec` (one real) or "
                "`[sec,nsec]` (two integers)");
    }
    tao_shared_array_set_timestamp(arr, idx, tao_time_normalize(&t));
    yarg_drop(argc - 1);
}

void Y_tao_get_image_shmid(int argc)
{
    if (argc != 2) {
        y_error("expecting exactly 2 arguments");
    }
    ypush_long(
        tao_remote_camera_get_image_shmid(
            ytao_get_remote_camera(1), ygets_l(0)));
}

static tao_serial remote_camera_config(
    ytao_object* yobj,
    int argc)
{
    // Check argument.
    if (yobj->lock != YTAO_LOCK_NONE) {
        y_error("remote camera must not be locked");
    }
    tao_remote_camera* cam = yobj->shared;

    // Safely copy the current configuration.
    tao_camera_config cfg;
    tao_status status = tao_remote_camera_lock(cam);
    if (status == TAO_OK) {
        yobj->lock = YTAO_LOCK_READWRITE;
        if (tao_remote_camera_get_configuration(cam, &cfg) != TAO_OK) {
            status = TAO_ERROR;
        }
        if (tao_remote_camera_unlock(cam) != TAO_OK) {
            status = TAO_ERROR;
        }
        yobj->lock = YTAO_LOCK_NONE;
    }
    if (status != TAO_OK) {
        ytao_throw_last_error();
    }

    // Parse positional arguments and other keywords.  Positional arguments
    // have been already parsed.
    double secs = WAIT_FOREVER;
    int npos = 0;
    while (--argc >= 0) {
        long index = yarg_key(argc);
        if (index < 0) {
            // Positional argument.
            ++npos;
            if (npos == 1) {
                if (!yarg_nil(argc)) {
                    secs = ygets_d(argc);
                }
            } else {
                y_error("too many positional arguments");
            }
        } else {
            // Keyword argument.
            --argc;
#define GET_VAL(name, path, getter)             \
            if (index == name##_index) {        \
                if (!yarg_nil(argc)) {          \
                    cfg.path = getter(argc);    \
                }                               \
                continue;                       \
            }
            GET_VAL(buffers,       buffers,       ygets_l);
            GET_VAL(exposuretime,  exposuretime,  ygets_d);
            GET_VAL(framerate,     framerate,     ygets_d);
            GET_VAL(height,        roi.height,    ygets_l);
            GET_VAL(pixeltype,     pixeltype,     get_pixeltype);
            GET_VAL(preprocessing, preprocessing, get_preprocessing);
            GET_VAL(rawencoding,   rawencoding,   get_encoding);
            GET_VAL(width,         roi.width,     ygets_l);
            GET_VAL(xbin,          roi.xbin,      ygets_l);
            GET_VAL(xoff,          roi.xoff,      ygets_l);
            GET_VAL(ybin,          roi.ybin,      ygets_l);
            GET_VAL(yoff,          roi.yoff,      ygets_l);
#undef GET_VAL
            // Maybe a named attribute.
            const char* key = yfind_name(index);
            long j = tao_camera_config_try_find_attr(&cfg, key);
            if (j < 0) {
                ytao_throw("unsupported configuration setting `%s`", key);
            }
            tao_attr* attr = (tao_attr*)tao_camera_config_unsafe_get_attr(&cfg, j);
            if (!TAO_ATTR_IS_WRITABLE(attr)) {
                ytao_throw("attribute `%s` is not writable", key);
            }
            switch (TAO_ATTR_TYPE(attr)) {
            case TAO_ATTR_BOOLEAN:
                attr->val.b = (yarg_true(argc) ? true : false);
                break;
            case TAO_ATTR_INTEGER:
                if (ytao_is_scalar_integer(argc)) {
                    attr->val.i = ygets_l(argc);
                } else {
                    ytao_throw("value of `%s` must be a scalar integer", key);
                }
                break;
            case TAO_ATTR_FLOAT:
                if (ytao_is_scalar_real(argc)) {
                    attr->val.f = ygets_d(argc);
                } else {
                    ytao_throw("value of `%s` must be a scalar real", key);
                }
                break;
            case TAO_ATTR_STRING:
                if (ytao_is_scalar_string(argc)) {
                    const char* str = ygets_q(argc);
                    long len = TAO_STRLEN(str);
                    long maxlen = tao_attr_get_max_val_size(attr);
                    if (len >= maxlen) {
                        // Silently truncate string (maxlen includes final null).
                        len = maxlen - 1;
                    }
                    for (long i = 0; i < len; ++i) {
                        attr->val.s[i] = str[i];
                    }
                    attr->val.s[len] = 0;
                } else {
                    ytao_throw("value of `%s` must be a scalar string", key);
                }
                break;
            default:
                ytao_throw("unknown type of value for named attribute `%s`",
                           key);
            }
        }
    }

    return tao_remote_camera_configure(cam, &cfg, secs);
}

void Y_tao_configure(int argc)
{
    if (--argc < 0) y_error("expecting at least one argument");
    if (yget_obj(argc, NULL) != ytao_object_type.type_name) {
        y_error("first argument must be a TAO object");
    }
    ytao_object* yobj = yget_obj(argc, &ytao_object_type);
    tao_serial ans = -1;
    if (yobj->ops == &ytao_remote_camera_ops) {
        ans = remote_camera_config(yobj, argc);
    } else {
        y_error("unexpected object type for `configure` command");
    }
    if (ans < 0) ytao_throw_last_error();
    ypush_long(ans);
}

void Y_tao_private(int argc)
{
    if (argc < 1 || argc > 2) y_error("expecting 1 or 2 arguments");
    ytao_object* yobj = ytao_get_object(argc - 1);
    if (argc == 1) {
        if (yobj->private == NULL) {
            ypush_nil();
        } else {
            ykeep_use(yobj->private);
        }
    } else {
        int iarg = 0;
        if (yobj->private != NULL) {
            void* private = yobj->private;
            yobj->private = NULL;
            if (yarg_subroutine()) {
                ydrop_use(private);
            } else {
                ypush_use(private);
                ++iarg;
            }
        }
        yobj->private = yget_use(iarg);
    }
}

static inline void push_command_result(tao_status status)
{
    if (status == TAO_OK) {
        ypush_int(1);
    } else if (status == TAO_TIMEOUT) {
        if (yarg_subroutine()) y_error("timeout");
        ypush_int(0);
    } else {
        ytao_throw_last_error();
    }
}

static ytao_object* initialize_simple_remote_command(
    int argc,
    double* secs)
{
    // Retrieve argument(s).
    if (argc < 1 || argc > 2) y_error("expecting 1 or 2 arguments");
    ytao_object* yobj = ytao_get_object(--argc);
    if (yobj->lock != YTAO_LOCK_NONE) y_error("object must not be locked");
    *secs = get_optional_timeout(--argc);

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Return object.
    return yobj;
}

void Y_tao_start(int argc)
{
    // Parse arguments.
    double secs;
    ytao_object* yobj = initialize_simple_remote_command(argc, &secs);

    // Send command.
    if (yobj->ops == &ytao_remote_camera_ops) {
       tao_serial ans = tao_remote_camera_start(
           (tao_remote_camera*)yobj->shared, secs);
       if (ans < 0) ytao_throw_last_error();
       ypush_long(ans);
    } else {
        y_error("unexpected object type for `start` command");
    }
}

void Y_tao_stop(int argc)
{
    // Parse arguments.
    double secs;
    ytao_object* yobj = initialize_simple_remote_command(argc, &secs);

    // Send command.
    if (yobj->ops == &ytao_remote_camera_ops) {
       tao_serial ans = tao_remote_camera_stop(
           (tao_remote_camera*)yobj->shared, secs);
       if (ans < 0) ytao_throw_last_error();
       ypush_long(ans);
    } else {
        y_error("unexpected object type for `stop` command");
    }
}

void Y_tao_abort(int argc)
{
    // Parse arguments.
    double secs;
    ytao_object* yobj = initialize_simple_remote_command(argc, &secs);

    // Send command.
    if (yobj->ops == &ytao_remote_camera_ops) {
       tao_serial ans = tao_remote_camera_abort(
           (tao_remote_camera*)yobj->shared, secs);
       if (ans < 0) ytao_throw_last_error();
       ypush_long(ans);
    } else {
        y_error("unexpected object type for `abort` command");
    }
}

void Y_tao_reset(int argc)
{
    // Parse arguments.
    double secs;
    ytao_object* yobj = initialize_simple_remote_command(argc, &secs);

    // Send command.
    if (yobj->ops == &ytao_remote_camera_ops) {
        tao_serial cmdnum = tao_remote_camera_reset(
            (tao_remote_camera*)yobj->shared, secs);
        if (cmdnum < 0) ytao_throw_last_error();
        ypush_long(cmdnum);
    } else if (yobj->ops == &ytao_remote_mirror_ops) {
        tao_serial mark = 0, datnum, cmdnum = tao_remote_mirror_reset(
            (tao_remote_mirror*)yobj->shared, mark, secs, &datnum);
        if (cmdnum < 0) ytao_throw_last_error();
        ytao_serial_pair_push(cmdnum, datnum);
    } else {
        y_error("unexpected object type");
    }
}

void Y_tao_kill(int argc)
{
    // Parse arguments.
    double secs;
    ytao_object* yobj = initialize_simple_remote_command(argc, &secs);

    // Send command.
    tao_serial ans = -1;
    if (yobj->ops == &ytao_remote_camera_ops) {
        ans = tao_remote_camera_kill(
            (tao_remote_camera*)yobj->shared, secs);
    } else if (yobj->ops == &ytao_remote_mirror_ops) {
        ans = tao_remote_mirror_kill(
            (tao_remote_mirror*)yobj->shared, secs);
    } else {
        y_error("unexpected object type");
    }
    if (ans < 0) ytao_throw_last_error();
    ypush_long(ans);
}

static void set_remote_mirror_offsets(
    int argc,
    tao_serial (*func)(tao_remote_mirror* obj, const double* vals,
                       long nvals, double secs, tao_serial* datnum))
{
    // Retrieve argument(s).
    if (argc < 2 || argc > 3) y_error("expecting 2 or 3 arguments");
    ytao_object* yobj = ytao_get_object(--argc);
    if (yobj->lock != YTAO_LOCK_NONE) y_error("object must not be locked");
    long nvals;
    const double* vals = ygeta_d(--argc, &nvals, NULL);
    double secs = get_optional_timeout(--argc);

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Execute command according to object type.
    if (yobj->ops == &ytao_remote_mirror_ops) {
        tao_remote_mirror* obj = yobj->shared;
        tao_serial datnum, cmdnum = func(obj, vals, nvals, secs, &datnum);
        if (cmdnum < 0) ytao_throw_last_error();
        ytao_serial_pair_push(cmdnum, datnum);
    } else {
        y_error("unexpected object type");
    }
}

void Y_tao_set_reference(int argc)
{
    set_remote_mirror_offsets(argc, tao_remote_mirror_set_reference);
}

void Y_tao_set_perturbation(int argc)
{
    set_remote_mirror_offsets(argc, tao_remote_mirror_set_perturbation);
}

void Y_tao_send_commands(int argc)
{
    // Retrieve argument(s).
    if (argc < 2 || argc > 4) y_error("expecting 2, 3, or 4 arguments");
    ytao_object* yobj = ytao_get_object(--argc);
    if (yobj->lock != YTAO_LOCK_NONE) y_error("object must not be locked");
    if (--argc < 0) y_error("missing reference values argument");
    long nvals;
    const double* vals = ygeta_d(argc, &nvals, NULL);
    double secs = get_optional_timeout(--argc);
    bool mark_unspecified = (--argc < 0 || yarg_nil(argc));
    tao_serial mark = mark_unspecified ? 0 : ygets_l(argc);

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Execute command according to object type.
    if (yobj->ops == &ytao_remote_mirror_ops) {
        tao_remote_mirror* obj = yobj->shared;
        if (mark_unspecified) {
            mark = tao_remote_mirror_get_mark(obj) + 1;
        }
        tao_serial datnum, cmdnum = tao_remote_mirror_send_commands(
            obj, vals, nvals, mark, secs, &datnum);
        if (cmdnum < 0) ytao_throw_last_error();
        ytao_serial_pair_push(cmdnum, datnum);
    } else {
        y_error("unexpected object type");
    }
}

void Y_tao_wait_output(int argc)
{
    // Retrieve argument(s).
    if (argc < 2 || argc > 3) y_error("expecting 2 or 3 arguments");
    ytao_object* yobj = ytao_get_object(--argc);
    if (yobj->lock != YTAO_LOCK_NONE) y_error("object must not be locked");
    tao_serial n = ytao_get_serial_number(--argc, 1);
    double secs = get_optional_timeout(--argc);

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Call method.
    tao_serial ans = -1;
    if (yobj->ops == &ytao_remote_mirror_ops) {
        ans = tao_remote_mirror_wait_output(
            (tao_remote_mirror*)yobj->shared, n, secs);
    } else if (yobj->ops == &ytao_remote_camera_ops) {
        ans = tao_remote_camera_wait_output(
            (tao_remote_camera*)yobj->shared, n, secs);
    } else {
        y_error("unexpected object type");
    }
    if (ans == -3) ytao_throw_last_error();
    ypush_long(ans);
}

void Y_tao_wait_command(int argc)
{
    // Retrieve argument(s).
    if (argc < 2 || argc > 3) y_error("expecting 2 or 3 arguments");
    ytao_object* yobj = ytao_get_object(--argc);
    if (yobj->lock != YTAO_LOCK_NONE) y_error("object must not be locked");
    tao_serial n = ytao_get_serial_number(--argc, 0);
    if (n < 1) y_error("invalid command serial number");
    double secs = get_optional_timeout(--argc);

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Call method.
    tao_status status = TAO_OK;
    if (yobj->ops == &ytao_remote_mirror_ops) {
        status = tao_remote_mirror_wait_command(
            (tao_remote_mirror*)yobj->shared, n, secs);
    } else if (yobj->ops == &ytao_remote_camera_ops) {
        status = tao_remote_camera_wait_command(
            (tao_remote_camera*)yobj->shared, n, secs);
    } else {
        y_error("unexpected object type");
    }
    push_command_result(status);
}

void Y_tao_find_mark(int argc)
{
    if (argc != 2) y_error("syntax: tao_find_mark(obj, mrk)");
    ytao_object* yobj = ytao_get_object(1);
    tao_serial mrk = get_serial_number(0);
    tao_serial num = 0;
    if (yobj->ops == &ytao_remote_mirror_ops) {
        num = tao_remote_mirror_find_mark((tao_remote_mirror*)yobj->shared, mrk);
    }
    ypush_long(num);
}

void Y_tao_wait_frame(int argc)
{
    // Retrieve argument(s).
    if (argc < 2 || argc > 3) y_error("syntax: tao_wait_frame(obj, num[, secs])");
    int iarg = argc;
    ytao_object* yobj = ytao_get_object(--iarg);
    tao_serial serial = get_serial_number(--iarg);
    double secs = get_optional_timeout(--iarg);

    // Object must not be already locked.
    if (yobj->lock != YTAO_LOCK_NONE) {
        y_error("object must not be locked");
    }

    // Figure out if called as a subroutine (before waiting to save some time).
    bool subroutine = yarg_subroutine();

    // Check that there are no pending signals before waiting.
    if (p_signalling) {
        p_abort();
    }

    // Execute command according to object type.
    if (yobj->ops->on_wait_data_frame == NULL) {
         y_error("unexpected object type");
    } else {
        yobj->ops->on_wait_data_frame(yobj, serial, secs, subroutine);
    }
}

void Y_tao_get_current_time(int argc)
{
    tao_time t;
    if (tao_get_current_time(&t) != TAO_OK) {
        ytao_throw_last_error();
    }
    ytao_push_time(&t);
}

void Y_tao_get_monotonic_time(int argc)
{
    tao_time t;
    if (tao_get_monotonic_time(&t) != TAO_OK) {
        ytao_throw_last_error();
    }
    ytao_push_elapsed_time(&t);
}

void Y_tao_get_monotonic_time_origin(int argc)
{
    ytao_push_time(&ytao_monotonic_origin);
}

void Y_tao_sleep(int argc)
{
    if (argc != 1) y_error("expecting exactly 1 argument");
    tao_sleep(ygets_d(0));
    ypush_nil();
}

void Y_tao_debug(int argc)
{
    if (argc != 1) y_error("expecting exactly 1 argument");
    int oldval = debug ? 1 : 0;
    if (! yarg_nil(0)) debug = (yarg_true(0) != 0);
    ypush_int(oldval);
}

void Y_tao_format_encoding(int argc)
{
    char buf[TAO_ENCODING_STRING_SIZE];
    if (argc != 1) y_error("expecting exactly 1 argument");
    if (tao_format_encoding(buf, ygets_l(0)) != TAO_OK) {
        ytao_throw_last_error();
    }
    ytao_push_string(buf);
}

void Y_tao_parse_encoding(int argc)
{
    if (argc != 1) y_error("expecting exactly 1 argument");
    ypush_long(tao_parse_encoding(ygets_q(0)));
}

void Y_tao_format_preprocessing(int argc)
{
    ytao_push_string(format_preprocessing(ygets_l(0)));
}

void Y_tao_parse_preprocessing(int argc)
{
    if (argc != 1) y_error("expecting exactly 1 argument");
    ypush_long(parse_preprocessing(ygets_q(0)));
}

static void initialize_type(
    size_t size,
    int ytype,
    bool issigned,
    bool floatingpoint)
{
    // MAcros to work with the non-const arrays.
#define TO_YORICK_ELTYPE(T) to_yorick_eltype_[(T) - TAO_INT8]
#define TO_TAO_ELTYPE(T)       to_tao_eltype_[(T) - Y_CHAR]
    if (floatingpoint) {
        // Floating-point type.
        if (size == sizeof(float)) {
            TO_YORICK_ELTYPE(TAO_FLOAT) = ytype;
            TO_TAO_ELTYPE(ytype) = TAO_FLOAT;
        } else if (size == sizeof(double)) {
            TO_YORICK_ELTYPE(TAO_DOUBLE) = ytype;
            TO_TAO_ELTYPE(ytype) = TAO_DOUBLE;
        }
    } else {
        // Integer type.
        if (size == 1) {
            TO_YORICK_ELTYPE(TAO_INT8) = ytype;
            TO_YORICK_ELTYPE(TAO_UINT8) = ytype;
            TO_TAO_ELTYPE(ytype) = (issigned ? TAO_INT8 : TAO_UINT8);
        } else if (size == 2) {
            TO_YORICK_ELTYPE(TAO_INT16) = ytype;
            TO_YORICK_ELTYPE(TAO_UINT16) = ytype;
            TO_TAO_ELTYPE(ytype) = (issigned ? TAO_INT16 : TAO_UINT16);
        } else if (size == 4) {
            TO_YORICK_ELTYPE(TAO_INT32) = ytype;
            TO_YORICK_ELTYPE(TAO_UINT32) = ytype;
            TO_TAO_ELTYPE(ytype) = (issigned ? TAO_INT32 : TAO_UINT32);
        } else if (size == 8) {
            TO_YORICK_ELTYPE(TAO_INT64) = ytype;
            TO_YORICK_ELTYPE(TAO_UINT64) = ytype;
            TO_TAO_ELTYPE(ytype) = (issigned ? TAO_INT64 : TAO_UINT64);
        }
    }
#undef TO_YORICK_ELTYPE
#undef TO_TAO_ELTYPE
}

static inline void initialize_ops(
    ytao_operations* ops,
    const ytao_operations* parent)
{
    if (ops->on_eval == NULL) {
        ops->on_eval = parent->on_eval;
    }
    if (ops->on_extract == NULL) {
        ops->on_extract = parent->on_extract;
    }
}

void Y__tao_init(int argc)
{
    // Check some assertions.
    long nerrs = 0; // to count assertion errors
    if (sizeof(float32_t) != 4) {
        ++nerrs;
        fprintf(stderr, "Assertion failed: sizeof(float32_t) = %zu != 4\n",
                sizeof(float32_t));
    }
    if (sizeof(float64_t) != 8) {
        ++nerrs;
        fprintf(stderr, "Assertion failed: sizeof(float64_t) = %zu != 4\n",
                sizeof(float64_t));
    }

    // Check assertions about TAO types.
    struct {
        const char* name;
        tao_eltype  value;
    } ttypes[] = {
#define ELEM(x) { #x, x }
        ELEM(TAO_INT8),
        ELEM(TAO_UINT8),
        ELEM(TAO_INT16),
        ELEM(TAO_UINT16),
        ELEM(TAO_INT32),
        ELEM(TAO_UINT32),
        ELEM(TAO_INT64),
        ELEM(TAO_UINT64),
        ELEM(TAO_FLOAT),
        ELEM(TAO_DOUBLE)
#undef ELEM
    };
    for (int i = 0; i < LENGTH(ttypes); ++i) {
        if (ttypes[i].value < 0) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: TAO type %s = %d is less "
                    "than zero.\n", ttypes[i].name, ttypes[i].value);
        }
        if (ttypes[i].value < TAO_INT8) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: TAO type %s = %d is less than "
                    "TAO_INT8 = %d\n", ttypes[i].name, ttypes[i].value,
                    TAO_INT8);
        }
        if (ttypes[i].value > TAO_DOUBLE) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: TAO type %s = %d is greater "
                    "than TAO_DOUBLE = %d\n", ttypes[i].name, ttypes[i].value,
                    TAO_DOUBLE);
        }
    }
    if (TAO_DOUBLE != TAO_INT8 + LENGTH(ttypes) - 1) {
        ++nerrs;
        fprintf(stderr, "Assertion failed: TAO type TAO_DOUBLE = %d is not "
                "equal to TAO_INT8 + %ld = %ld\n", TAO_DOUBLE,
                (long)(LENGTH(ttypes) - 1),
                (long)(TAO_INT8 + LENGTH(ttypes) - 1));
    }

    // Check assertions about TAO types.
    struct {
        const char* name;
        int        value;
    } ytypes[] = {
#define ELEM(x) { #x, x }
        ELEM(Y_CHAR),
        ELEM(Y_SHORT),
        ELEM(Y_INT),
        ELEM(Y_LONG),
        ELEM(Y_FLOAT),
        ELEM(Y_DOUBLE)
#undef ELEM
    };
    for (int i = 0; i < LENGTH(ytypes); ++i) {
        if (ytypes[i].value < 0) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: Yorick type %s = %d is less "
                    "than zero.\n", ytypes[i].name, ytypes[i].value);
        }
        if (ytypes[i].value < Y_CHAR) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: Yorick type %s = %d is less "
                    "than Y_CHAR = %d\n", ytypes[i].name, ytypes[i].value,
                    Y_CHAR);
        }
        if (ytypes[i].value > Y_DOUBLE) {
            ++nerrs;
            fprintf(stderr, "Assertion failed: Yorick type %s = %d is greater "
                    "than Y_DOUBLE = %d\n", ytypes[i].name, ytypes[i].value,
                    Y_DOUBLE);
        }
    }
    if (Y_DOUBLE != Y_CHAR + LENGTH(ytypes) - 1) {
        ++nerrs;
        fprintf(stderr, "Assertion failed: Yorick type Y_DOUBLE = %d is not "
                "equal to Y_CHAR + %ld = %ld\n", Y_DOUBLE,
                (long)(LENGTH(ytypes) - 1),
                (long)(Y_CHAR + LENGTH(ytypes) - 1));
    }

    // Exit if any assertion failed.
    if (nerrs > 0) {
        ytao_throw("%ld assertion(s) failed, code must be fixed", nerrs);
    }

    // Initialize tables for type equivalences.
    ytao_to_yorick_eltype_ = to_yorick_eltype_ - TAO_INT8;
    ytao_to_tao_eltype_ = to_tao_eltype_ - Y_CHAR;
    for (int i = 0; i < LENGTH(to_yorick_eltype_); ++i) {
        to_yorick_eltype_[i] = -1;
    }
    for (int i = 0; i < LENGTH(to_tao_eltype_); ++i) {
        to_tao_eltype_[i] = -1;
    }
    initialize_type(sizeof(char),   Y_CHAR,   IS_SIGNED(char),  false);
    initialize_type(sizeof(short),  Y_SHORT,  IS_SIGNED(short), false);
    initialize_type(sizeof(int),    Y_INT,    IS_SIGNED(int),   false);
    initialize_type(sizeof(long),   Y_LONG,   IS_SIGNED(long),  false);
    initialize_type(sizeof(float),  Y_FLOAT,  true,             true);
    initialize_type(sizeof(double), Y_DOUBLE, true,             true);


    // Initialize tables of operations.  Order is important.  We start with
    // most basic types.
    //
    // - Initialize table of operations of basic r/w locked objects.
    initialize_ops(&ytao_rwlocked_object_ops,
                   &ytao_shared_object_ops);
    //
    // - Initialize table of operations of basic remote objects.
    initialize_ops(&ytao_remote_object_ops,
                   &ytao_shared_object_ops);
    //
    // - Initialize table of operations of shared arrays.
    initialize_ops(&ytao_shared_array_ops,
                   &ytao_rwlocked_object_ops);
    //
    // - Initialize table of operations of remote cameras.
    initialize_ops(&ytao_remote_camera_ops,
                   &ytao_remote_object_ops);
    //
    // - Initialize table of operations of remote mirrors.
    initialize_ops(&ytao_remote_mirror_ops,
                   &ytao_remote_object_ops);
    //
    // - Initialize table of operations of remote sensors.
    initialize_ops(&ytao_remote_sensor_ops,
                   &ytao_remote_object_ops);

    // Define some constants.
#define DEF_LONG_CONST(id)                      \
    do {                                        \
        ypush_long(id);                         \
        yput_global(yfind_global(#id, 0), 0);   \
        yarg_drop(1);                           \
    } while (false)
    DEF_LONG_CONST(TAO_BAD_SHMID);
    DEF_LONG_CONST(TAO_PERSISTENT);
    DEF_LONG_CONST(TAO_SHARED_MAGIC);
    DEF_LONG_CONST(TAO_SHARED_OBJECT);
    DEF_LONG_CONST(TAO_RWLOCKED_OBJECT);
    DEF_LONG_CONST(TAO_REMOTE_OBJECT);
    DEF_LONG_CONST(TAO_SHARED_ARRAY);
    DEF_LONG_CONST(TAO_REMOTE_CAMERA);
    DEF_LONG_CONST(TAO_REMOTE_MIRROR);
    DEF_LONG_CONST(TAO_REMOTE_SENSOR);
    DEF_LONG_CONST(TAO_INT8);
    DEF_LONG_CONST(TAO_UINT8);
    DEF_LONG_CONST(TAO_INT16);
    DEF_LONG_CONST(TAO_UINT16);
    DEF_LONG_CONST(TAO_INT32);
    DEF_LONG_CONST(TAO_UINT32);
    DEF_LONG_CONST(TAO_INT64);
    DEF_LONG_CONST(TAO_UINT64);
    DEF_LONG_CONST(TAO_FLOAT);
    DEF_LONG_CONST(TAO_DOUBLE);
#undef DEF_LONG_CONST
#define DEF_INT_CONST(id)                       \
    do {                                        \
        ypush_int(id);                          \
        yput_global(yfind_global(#id, 0), 0);   \
        yarg_drop(1);                           \
    } while (false)
    DEF_INT_CONST(TAO_STATE_INITIALIZING);
    DEF_INT_CONST(TAO_STATE_WAITING);
    DEF_INT_CONST(TAO_STATE_CONFIGURING);
    DEF_INT_CONST(TAO_STATE_STARTING);
    DEF_INT_CONST(TAO_STATE_WORKING);
    DEF_INT_CONST(TAO_STATE_STOPPING);
    DEF_INT_CONST(TAO_STATE_ABORTING);
    DEF_INT_CONST(TAO_STATE_ERROR);
    DEF_INT_CONST(TAO_STATE_RESETTING);
    DEF_INT_CONST(TAO_STATE_QUITTING);
    DEF_INT_CONST(TAO_STATE_UNREACHABLE);
    DEF_INT_CONST(TAO_PREPROCESSING_NONE);
    DEF_INT_CONST(TAO_PREPROCESSING_AFFINE);
    DEF_INT_CONST(TAO_PREPROCESSING_FULL);
#undef DEF_INT_CONST

#define DEF_DOUBLE_CONST(id, val)               \
    do {                                        \
        ypush_double(val);                      \
        yput_global(yfind_global(#id, 0), 0);   \
        yarg_drop(1);                           \
    } while (false)
    DEF_DOUBLE_CONST(TAO_MAX_TIME_SECONDS, MAX_SECONDS);
    DEF_DOUBLE_CONST(TAO_WAIT_FOREVER, WAIT_FOREVER);
#undef DEF_DOUBLE_CONST

    // Global indices.
#define DEF_GLOB_INDEX(x) if (x##_index == -1L) x##_index = yget_global(#x, 0)
    DEF_GLOB_INDEX(buffers);
    DEF_GLOB_INDEX(exposuretime);
    DEF_GLOB_INDEX(framerate);
    DEF_GLOB_INDEX(height);
    DEF_GLOB_INDEX(perms);
    DEF_GLOB_INDEX(pixeltype);
    DEF_GLOB_INDEX(preprocessing);
    DEF_GLOB_INDEX(rawencoding);
    DEF_GLOB_INDEX(width);
    DEF_GLOB_INDEX(xbin);
    DEF_GLOB_INDEX(xoff);
    DEF_GLOB_INDEX(ybin);
    DEF_GLOB_INDEX(yoff);
#undef DEF_GLOB_INDEX

    // If this is the first call, get the origin of time and the address of
    // thread's last error (Yorick has only one thread).
    if (ytao_last_error == NULL) {
        tao_get_monotonic_time(&ytao_monotonic_origin);
        ytao_last_error = tao_get_last_error();
    }

    ypush_nil();
}
