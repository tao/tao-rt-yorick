/*
 * tao-rt-tests.i --
 *
 * Unit testing for TAO real-time interface in Yorick
 *
 *-----------------------------------------------------------------------------
 *
 * This file is part of the TAO real-time software licensed under the MIT
 * license (https://git-cral.univ-lyon1.fr/tao/tao-rt-yorick).
 *
 * Copyright (C) 2024, Éric Thiébaut.
 */

require, "tao-rt.i";
require, "testing.i";
test_init, 1n;

// Library version.
test_eval, "is_array(tao_library_version())";
test_eval, "structof(tao_library_version()) == long";
test_eval, "allof(dimsof(tao_library_version()) == [1,3])";
test_eval, "tao_library_version(        1) == tao_library_version()(1)";
test_eval, "tao_library_version(        2) == tao_library_version()(2)";
test_eval, "tao_library_version(        3) == tao_library_version()(3)";
test_eval, "tao_library_version(       -2) == tao_library_version()(1)";
test_eval, "tao_library_version(       -1) == tao_library_version()(2)";
test_eval, "tao_library_version(        0) == tao_library_version()(3)";
test_eval, "tao_library_version(\"major\") == tao_library_version()(1)";
test_eval, "tao_library_version(\"minor\") == tao_library_version()(2)";
test_eval, "tao_library_version(\"patch\") == tao_library_version()(3)";

// Summary.
test_summary;
