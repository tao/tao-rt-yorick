/* This check requires to have launched the fake dm by the following command:
* tao_fake_mirror_server "fakedm"
*/
dm = tao_attach_remote_mirror("fakedm");

/* Test reference commands */
refs = (random(dm.nacts) - 0.5)*0.01;
cmdnum=tao_set_reference(dm, refs, 0.01);
tao_wait_command, dm, cmdnum; // wait for the reference commands to be taken
if (anyof(dm.refcmds!=refs)) error, "some referene values are different";

/* Test requested and effective commands */
cmds = (random(dm.nacts) - 0.5)*0.01;
ser = dm.serial;
cmdnum = tao_send_commands(dm, cmds, 0.01);
frm = tao_wait_frame(dm, cmdnum(2), dm_timeout);

if (anyof(frm.refcmds != refs)) error, "some reference values are different in frame";
if (max(abs(frm.reqcmds - cmds)) > 1e-15) error, "some requested values are different";
if (max(abs(frm.devcmds - (cmds + refs))) > 1e-15) error, "some effective values are different";

/* test also serial number */
if (dm.serial != ser + 1) error, "unexpected serial number in DM";
if (frm.serial != ser + 1) error, "unexpected serial number in frame";
