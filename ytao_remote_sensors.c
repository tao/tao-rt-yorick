// ytao_remote_sensors.c -
//
// Implements Yorick interface for TAO remote wavefront sensors.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2018-2024, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include <limits.h>

#include "ytao.h"
#include "macros.h"

#include <tao-remote-sensors-private.h>

//-----------------------------------------------------------------------------
// WAVEFRONT SENSOR DATA FRAME OBJECT

typedef struct sensor_data_frame sensor_data_frame;

struct sensor_data_frame {
    tao_dataframe_info        info;// Data frame information.
    long                     nsubs;// Number of subimages.
    tao_shackhartmann_data vals[1];// Reference values and commands
};

#if 0 // FIXME: unused
static void free_sensor_data_frame(void* addr)
{
    // Nothing to do.
}
#endif

#if 0 // FIXME: unused
static void print_sensor_data_frame(void* addr)
{
    sensor_data_frame* frame = addr;
    tao_buffer_clear(&ytao_dynbuf);
    if (tao_buffer_printf(
            &ytao_dynbuf, "wavefront sensor data frame "
            "(nsubs=%ld, serial=%lld, mark=%lld, time=%lld.%09lds)",
            (long      int)frame->nsubs,
            (long long int)frame->info.serial,
            (long long int)frame->info.mark,
            (long long int)frame->info.time.sec,
            (long      int)frame->info.time.nsec) != TAO_OK) {
        ytao_throw_last_error();
    }
    y_print(tao_buffer_get_contents(&ytao_dynbuf, NULL), 1);
    tao_buffer_clear(&ytao_dynbuf);
}
#endif

#if 0 // FIXME: unused
static void extract_sensor_data_frame(void* addr, char* name)
{
    size_t len = strlen(name);
    sensor_data_frame* frame = addr;
    switch (name[0]) {
    case 'a':
        if (match("alpha", 5)) {
            long nsubs = frame->nsubs;
            long dims[2] = {1, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = SIZE_OF_MEMBER(tao_shackhartmann_data, alpha);
            if (elsize == sizeof(float)) {
                float* dst = ypush_f(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[i] = src[i].alpha;
                }
            } else if (elsize == sizeof(double)) {
                double* dst = ypush_d(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[i] = src[i].alpha;
                }
            } else {
                ytao_throw("no %zu-bits floating-point type", 8*elsize);
            }
            return;
        }
        break;
    case 'b':
        if (match("box", 3)) {
            long nsubs = frame->nsubs;
            long dims[3] = {1, 4, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = sizeof(src->box.xmin);
            tao_bounding_box* dst = ytao_push_integers(elsize, dims);
            for (long i = 0; i < nsubs; ++i) {
                dst[i] = src[i].box;
            }
            return;
        }
       break;
    case 'd':
        if (match("dat", 3)) {
            long nsubs = frame->nsubs;
            long dims[3] = {1, 2, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = sizeof(src->pos.x);
            if (elsize == sizeof(float)) {
                float* dst = ypush_f(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[2*i  ] = src[i].pos.x;
                    dst[2*i+1] = src[i].pos.y;
                }
            } else if (elsize == sizeof(double)) {
                double* dst = ypush_d(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[2*i  ] = src[i].pos.x;
                    dst[2*i+1] = src[i].pos.y;
                }
            } else {
                ytao_throw("no %zu-bits floating-point type", 8*elsize);
            }
            return;
        }
        break;
    case 'e':
        if (match("eta", 3)) {
            long nsubs = frame->nsubs;
            long dims[2] = {1, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = SIZE_OF_MEMBER(tao_shackhartmann_data, eta);
            if (elsize == sizeof(float)) {
                float* dst = ypush_f(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[i] = src[i].eta;
                }
            } else if (elsize == sizeof(double)) {
                double* dst = ypush_d(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[i] = src[i].eta;
                }
            } else {
                ytao_throw("no %zu-bits floating-point type", 8*elsize);
            }
            return;
        }
        break;
    case 'm':
        if (match("mark", 4)) {
            ypush_long(frame->info.mark);
            return;
        }
        break;
    case 'n':
        if (match("nsubs", 5)) {
            ypush_long(frame->nsubs);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->flags & 0x1ff);
            return;
        }
        break;
    case 'r':
        if (match("ref", 3)) {
            long nsubs = frame->nsubs;
            long dims[3] = {1, 2, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = sizeof(src->ref.x);
            tao_position* dst = ytao_push_floats(elsize, dims);
            for (long i = 0; i < nsubs; ++i) {
                dst[i] = src[i].ref;
            }
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(frame->info.serial);
            return;
        }
        break;
    case 't':
        if (match("time", 4)) {
            ytao_push_time(&frame->info.time);
            return;
        }
        break;
    case 'w':
        if (match("wgt", 3)) {
            long nsubs = frame->nsubs;
            long dims[3] = {1, 3, nsubs};
            const tao_shackhartmann_data* src = frame->vals;
            size_t elsize = sizeof(src->pos.wxx);
            if (elsize == sizeof(float)) {
                float* dst = ypush_f(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[3*i  ] = src[i].pos.wxx;
                    dst[3*i+1] = src[i].pos.wxy;
                    dst[3*i+2] = src[i].pos.wyy;
                }
            } else if (elsize == sizeof(double)) {
                double* dst = ypush_d(dims);
                for (long i = 0; i < nsubs; ++i) {
                    dst[3*i  ] = src[i].pos.wxx;
                    dst[3*i+1] = src[i].pos.wxy;
                    dst[3*i+2] = src[i].pos.wyy;
                }
            } else {
                ytao_throw("no %zu-bits floating-point type", 8*elsize);
            }
            return;
        }
        break;
    }
    y_error("bad member");
}
#endif

#if 0 // FIXME: unused
static y_userobj_t sensor_data_frame_type = {
    "tao_sensor_data_frame",
    free_sensor_data_frame,
    print_sensor_data_frame,
    NULL, // on_eval
    extract_sensor_data_frame,
    NULL
};
#endif

#if 0 // FIXME: unused
static sensor_data_frame* push_sensor_data_frame(long nsubs)
{
    size_t size = TAO_OFFSET_OF(sensor_data_frame, vals)
        + nsubs*sizeof(tao_shackhartmann_data);
    sensor_data_frame* frame = ypush_obj(&sensor_data_frame_type, size);
    frame->nsubs = nsubs;
    return frame;
}
#endif

#if 0 // FIXME: unused
static sensor_data_frame* get_sensor_data_frame(int iarg)
{
    if (iarg < 0) y_error("missing sensor data frame argument");
    return yget_obj(iarg, &sensor_data_frame_type);
}
#endif

//-----------------------------------------------------------------------------
// REMOTE SENSOR OBJECT

static void remote_sensor_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    const tao_remote_sensor* obj = yobj->shared;
    switch (name[0]) {
    case 'a':
        if (match("alive", 5)) {
            ypush_int(tao_remote_sensor_is_alive(obj));
            return;
        }
        break;
    case 'd':
        if (match("dims", 4)) {
            long dims[2] = {1,3};
            long* dst = ypush_l(dims);
            const long* src = tao_remote_sensor_get_dims(obj);
            dst[0] = 2;
            dst[1] = src[0];
            dst[2] = src[1];
            return;
        }
        break;
    case 'f':
        if (match("flags", 5)) {
            ypush_long(tao_remote_sensor_get_flags(obj));
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        if (match("layout", 6)) {
            long dims[3];
            const long* src = tao_remote_sensor_get_inds(obj, dims+1);
            dims[0] = 2;
            if (src != NULL && dims[1] > 0 && dims[2] > 0) {
                long* dst = ypush_l(dims);
                long n = dims[1]*dims[2];
                for (long i = 0; i < n; ++i) {
                    // Convert 0-based indices into 1-based indices.
                    dst[i] = src[i] + 1;
                }
            } else {
                ypush_nil();
            }
            return;
        }
        break;
    case 'n':
        if (match("nsubs", 5)) {
            ypush_long(tao_remote_sensor_get_nsubs(obj));
            return;
        }
        if (match("nbufs", 5)) {
            ypush_long(tao_remote_sensor_get_nbufs(obj));
            return;
        }
        if (match("ncmds", 5)) {
            ypush_long(tao_remote_sensor_get_ncmds(obj));
            return;
        }
        if (match("nrefs", 5)) {
            ypush_long(obj->base.base.nrefs);
            return;
        }
        break;
    case 'o':
        if (match("owner", 5)) {
            ytao_push_string(yobj->owner);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            ypush_long(tao_remote_sensor_get_flags(obj) & 0x1ff);
            return;
        }
        if (match("pid", 3)) {
            ypush_long(tao_remote_sensor_get_pid(obj));
            return;
        }
        break;
    case 'r':
        if (match("refs", 4)) {
            long nsubs;
            const tao_subimage* src = tao_remote_sensor_get_subs(obj, &nsubs);
            long dims[3] = {1, 2, nsubs};
            double* dst = ypush_d(dims);
            for (long i = 0; i < nsubs; ++i) {
                dst[2*i  ] = src[i].ref.x;
                dst[2*i+1] = src[i].ref.y;
            }
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(tao_remote_sensor_get_serial(obj));
            return;
        }
        if (match("state", 5)) {
            ypush_int(tao_remote_sensor_get_state(obj));
            return;
        }
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        break;
    case 't':
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    }
    y_error("bad member");
}

#if 0 // FIXME: unused
static void remote_sensor_wait_data_frame(
    ytao_object* yobj,
    tao_serial serial,
    double secs,
    bool subroutine)
{
    tao_remote_sensor* obj = yobj->shared;
    serial = tao_remote_sensor_wait_output(obj, serial, secs);
    if (serial < -2) {
        // An error occurred.
    error:
        ytao_throw_last_error();
        return;
    }
    if (subroutine) {
        if (serial < 1) {
            // Frame cannot be retrieved.
            const char* mesg = (serial == 0 ? "timeout" :
                                (serial == -1 ? "too late" :
                                 "server has been killed"));
            y_error(mesg);
        }
        return;
    }
    // Retrieve frame contents.
    long nsubs = tao_remote_sensor_get_nsubs(obj);
    sensor_data_frame* frame = push_sensor_data_frame(nsubs);
    if (serial < 1) {
        // Contents is not available.
        frame->info.serial = serial;
    } else {
        // Attempt to load contents.
        tao_status status = tao_remote_sensor_fetch_data(
            obj, serial, frame->vals, nsubs, &frame->info);
        if (status != TAO_OK) {
            if (status == TAO_TIMEOUT) {
                // Contents have been overwritten.
                frame->info.serial = -1;
            } else {
                goto error;
            }
        }
    }
}
#endif

ytao_operations ytao_remote_sensor_ops = {
    // TAO_REMOTE_SENSOR,
    false,
    NULL, // on_eval
    remote_sensor_extract,
    NULL, // remote_sensor_wait_data_frame
};
