- Commands `tao_set_reference`, `tao_set_perturbation`, and `tao_send_commands`
  should return a 2-tuple (or alike) with the command and data serial numbers.
  This can be another specialized object.  `tao_wait_command` and
  `tao_wait_output` can automatically extract the relevant field of this
  object.  This should also be the implkemented behavior in Julia.

- Make data-frame `frm` a special object with e specialized `FormOperand`
  virtual method.

- Speedup `frm.refs`, `frm.reqs`, `frm.offs`, and `frm.acts` by having them
  stored as references to Yorick arrays.

- For fine-tuning, have function `tao_wait` to just wait for a given data-frame
  to be available, function `tao_fetch` to fetch an available data-frame, and
  function `tao_wait_and_fetch` to do both.
