// ytao_shared_arrays.c -
//
// Yorick interface to TAO shared arrays.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-shared-arrays-private.h>

static const char* unsupported_eltype = "unsupported array element type";
static const char* invalid_index = "only arr(i), arr(), arr(start:stop:step), "
    "or arr(*) are allowed for a shared array";

// Yorick type equivalent to that of the element of a shared array.
static int get_equiv_eltype(tao_shared_array* arr)
{
    int type = tao_shared_array_get_eltype(arr);
    return YTAO_CHECK_TAO_ELTYPE(type) ? YTAO_TO_YORICK_ELTYPE(type) : -1;
}

static void push_shared_array_data(tao_shared_array* arr, bool flat)
{
    // Get dimensions.
    long dims[Y_DIMSIZE];
    int ndims;
    long nelem;
    if (flat) {
        ndims = 1;
        nelem = tao_shared_array_get_length(arr);
        dims[0] = ndims;
        dims[1] = nelem;
    } else {
        ndims = tao_shared_array_get_ndims(arr);
        if (ndims < 0) {
            y_error("invalid number of dimensions");
        }
        if (ndims >= Y_DIMSIZE) {
            y_error("too many dimensions");
        }
        dims[0] = ndims;
        nelem = 1;
        for (int d = 1; d <= ndims; ++d) {
            dims[d] = tao_shared_array_get_dim(arr, d);
            nelem *= dims[d];
        }
    }

    // Get element size and push result.
    int type = get_equiv_eltype(arr);
    void* dst;
    long elsize;
    switch (type) {
    case Y_CHAR:
        elsize = sizeof(char);
        dst = ypush_c(dims);
        break;
    case Y_SHORT:
        elsize = sizeof(short);
        dst = ypush_s(dims);
        break;
    case Y_INT:
        elsize = sizeof(int);
        dst = ypush_i(dims);
        break;
    case Y_LONG:
        elsize = sizeof(long);
        dst = ypush_l(dims);
        break;
    case Y_FLOAT:
        elsize = sizeof(float);
        dst = ypush_f(dims);
        break;
    case Y_DOUBLE:
        elsize = sizeof(double);
        dst = ypush_d(dims);
        break;
    default:
        y_error(unsupported_eltype);
        return;
    }

    // Copy elements.
    const void* src = tao_shared_array_get_data(arr);
    if (elsize > 0 && nelem > 0) {
        memcpy(dst, src, nelem*elsize);
    }
}

static void push_shared_array_range(tao_shared_array* arr, int rng, long mms[3])
{
    if (rng == RANGE_STAR) {
        // Range is `*`. Yield a flat array like `arr(*)`.
        push_shared_array_data(arr, true);
        return;
    }
    if ((rng & 15) == 1) {
        // Range is start:stop:step
        long start, step = mms[2], stop;
        if (step == 0) {
            y_error("invalid step in range (start:stop:step)");
        }
        int type = get_equiv_eltype(arr);
        long nelem = tao_shared_array_get_length(arr);
        if ((rng & Y_MIN_DFLT) == Y_MIN_DFLT) {
            start = step > 0 ? 1 : nelem;
        } else {
            start = mms[0];
            if (start <= 0) {
                start += nelem;
            }
        }
        if ((rng & Y_MAX_DFLT) == Y_MAX_DFLT) {
            stop = step > 0 ? nelem : 1;
        } else {
            stop = mms[1];
            if (stop <= 0) {
                stop += nelem;
            }
        }
        if (start < 1 || start > nelem || stop < 1 || stop > nelem) {
            y_error("index overreach beyond array bounds");
        }
        long n = step > 0 ?
            (stop - start + step)/step :
            (start - stop - step)/(-step);
        if (n < 1) {
            ypush_nil();
            return;
        }
        long dims[2] = {1, n};
        long j = start - 1;
        const void* data = tao_shared_array_get_data(arr);

#define EXTRACT(T, push)                                        \
        do {                                                    \
            T* dst = push(dims);                                \
            const T* src = data;                                \
            if (step == 1) {                                    \
                memcpy(dst, &src[j], n*sizeof(T));              \
            } else {                                            \
                for (long i = 0; i < n; ++i, j += step) {       \
                    dst[i] = src[j];                            \
                }                                               \
            }                                                   \
        } while (0)

        switch (type) {
        case Y_CHAR:
            EXTRACT(char, ypush_c);
            return;
        case Y_SHORT:
            EXTRACT(short, ypush_s);
            return;
        case Y_INT:
            EXTRACT(int, ypush_i);
            return;
        case Y_LONG:
            EXTRACT(long, ypush_l);
            return;
        case Y_FLOAT:
            EXTRACT(float, ypush_f);
            return;
        case Y_DOUBLE:
            EXTRACT(double, ypush_d);
            return;
        default:
            y_error(unsupported_eltype);
            return;
        }

#undef EXTRACT

    }

#if 0
    if (rng == RANGE_PLUS) {
        // Range is `+`.
    } else if (rng == RANGE_MINUS) {
        // Range is `-`.
    } else if (rng == RANGE_RUBBER) {
        // Range is `..`.
    }
#endif

    y_error(invalid_index);
}

static void push_shared_array_element(tao_shared_array* arr, long i)
{
    long n = tao_shared_array_get_length(arr);
    void* ptr = tao_shared_array_get_data(arr);
    if (i <= 0) {
        i += n;
    }
    if (i < 1 || i > n) {
        y_error("out of bound shared array index");
    }
    switch (tao_shared_array_get_eltype(arr)) {
    case TAO_INT8:
        ypush_int(((int8_t*)ptr)[i-1]);
        return;
    case TAO_UINT8:
        ypush_int(((uint8_t*)ptr)[i-1]);
        return;
    case TAO_INT16:
        ypush_int(((int16_t*)ptr)[i-1]);
        return;
    case TAO_UINT16:
        ypush_int(((uint16_t*)ptr)[i-1]);
        return;
    case TAO_INT32:
        if (sizeof(long) == 4) {
            ypush_long(((int32_t*)ptr)[i-1]);
        } else {
            ypush_int(((int32_t*)ptr)[i-1]);
        }
        return;
    case TAO_UINT32:
        if (sizeof(long) == 4) {
            ypush_long(((uint32_t*)ptr)[i-1]);
        } else {
            ypush_int(((uint32_t*)ptr)[i-1]);
        }
        return;
    case TAO_INT64:
        ypush_long(((int64_t*)ptr)[i-1]);
        return;
    case TAO_UINT64:
        ypush_long(((uint64_t*)ptr)[i-1]);
        return;
    case TAO_FLOAT:
        ypush_double(((float*)ptr)[i-1]);
        return;
    case TAO_DOUBLE:
        ypush_double(((double*)ptr)[i-1]);
        return;
    default:
        y_error(unsupported_eltype);
    }
}

static void shared_array_eval(ytao_object* yobj, int argc)
{
    tao_shared_array* arr = (tao_shared_array*)yobj->shared;
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    int eltype = yarg_typeid(0);
    if (eltype <= Y_LONG) {
        push_shared_array_element(arr, ygets_l(0));
        return;
    } else if (eltype == Y_VOID) {
        push_shared_array_data(arr, false);
        return;
    } else if (eltype == Y_RANGE) {
        long mms[3]; // min/max/step
        int rng = yget_range(0, mms);
        push_shared_array_range(arr, yget_range(0, mms), mms);
        return;
    }
    y_error(invalid_index);
}

static void shared_array_extract(ytao_object* yobj, const char* name)
{
    size_t len = strlen(name);
    tao_shared_array* arr = yobj->shared;
    switch (name[0]) {
    case 'd':
        if (match("data", 4)) {
            push_shared_array_data(arr, false);
            return;
        }
        if (match("dims", 4)) {
            long dimsofdims[2];
            long* dims;
            int d, ndims;
            ndims = tao_shared_array_get_ndims(arr);
            if (ndims < 0) {
                y_error("invalid number of dimensions");
            }
            dimsofdims[0] = 1;
            dimsofdims[1] = ndims + 1;
            dims = ypush_l(dimsofdims);
            dims[0] = ndims;
            for (d = 1; d <= ndims; ++d) {
                dims[d] = tao_shared_array_get_dim(arr, d);
            }
            return;
        }
        break;
    case 'e':
        if (match("eltype", 6)) {
            ypush_long(tao_shared_array_get_eltype(arr));
            return;
        }
        break;
    case 'f':
        if (match("flags", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->flags);
            return;
        }
        break;
    case 'l':
        if (match("lock", 4)) {
            ypush_int(yobj->lock);
            return;
        }
        break;
    case 'n':
        if (match("ndims", 5)) {
            ypush_long(tao_shared_array_get_ndims(arr));
            return;
        }
        if (match("nrefs", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->nrefs);
            return;
        }
        break;
    case 'p':
        if (match("perms", 5)) {
            tao_shared_object* obj = yobj->shared;
            ypush_long(obj->flags & 0x1ff);
            return;
        }
        break;
    case 's':
        if (match("serial", 6)) {
            ypush_long(tao_shared_array_get_serial(arr));
            return;
        }
        if (match("size", 4)) {
            ypush_long(yobj->size);
            return;
        }
        if (match("shmid", 5)) {
            ypush_long(yobj->shmid);
            return;
        }
        break;
    case 't':
        if (len == 10 && strncmp(name, "timestamp", 9) == 0) {
            int c_last = name[9];
            if (c_last == 's') {
                long dims[2] = {1,TAO_SHARED_ARRAY_TIMESTAMPS};
                double* vals = ypush_d(dims);
                tao_time t;
                for (int i = 0; i < TAO_SHARED_ARRAY_TIMESTAMPS; ++i) {
                    tao_shared_array_get_timestamp(arr, i, &t);
                    vals[i] = ytao_elapsed_time(&t);
                }
                return;
            } else {
                int i = c_last - '1';
                if (i >= 0 && i < TAO_SHARED_ARRAY_TIMESTAMPS) {
                    ytao_push_shared_array_timestamp(arr, i);
                    return;
                }
            }
        }
        if (match("type", 4)) {
            ypush_long(yobj->type);
            return;
        }
        break;
    }
    y_error("bad member");
}


ytao_operations ytao_shared_array_ops = {
    // TAO_SHARED_ARRAY,
    true,
    shared_array_eval,
    shared_array_extract,
    NULL, // on_wait_data_frame
};

//-----------------------------------------------------------------------------
// BUILTINS

void Y_tao_get_data(int argc)
{
    if (argc != 1) {
        y_error("expecting exactly 1 argument");
    }
    push_shared_array_data(ytao_get_shared_array(0), false);
}

void Y_tao_set_data(int argc)
{
    long dims[Y_DIMSIZE];
    tao_shared_array* arr;
    const void* src;
    void* dst;
    int src_type, d, ndims;
    long ntot;

    if (argc != 2) {
        y_error("expecting exactly 2 arguments");
    }
    arr = ytao_get_shared_array(argc - 1);
    tao_eltype dst_type = tao_shared_array_get_eltype(arr);
    if (! YTAO_CHECK_TAO_ELTYPE(dst_type)) {
        y_error("unsupported TAO shared array element type");
    }
    src = ygeta_any(argc - 2, &ntot, dims, &src_type);
    if (! YTAO_CHECK_YORICK_ELTYPE(src_type)) {
        y_error("unsupported Yorick array element type");
    }
    ndims = tao_shared_array_get_ndims(arr);
    if (dims[0] != ndims) {
        y_error("not same number of dimensions");
    }
    for (d = 1; d <= ndims; ++d) {
        long dim = tao_shared_array_get_dim(arr, d);
        if (dims[d] != dim) {
            y_error("not same number of dimensions");
        }
    }
    dst = tao_shared_array_get_data(arr);
    ytao_copy(dst, dst_type, src, src_type, ntot);
    yarg_drop(argc - 1);
}
