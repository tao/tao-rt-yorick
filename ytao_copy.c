// ytao-copy.c -
//
// Copy and convert values.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

typedef void copier(void* dst, const void* src, long n);

#define COPY(sfx, dst_type, src_type)                           \
    static void copy_##sfx(void* dst, const void* src, long n)  \
    {                                                           \
        dst_type* dst_ = dst;                                   \
        const src_type* src_ = src;                             \
        for (long i = 0; i < n; ++i) {                          \
            dst_[i] = src_[i];                                  \
        }                                                       \
    }

COPY(  char_i8,     int8_t, char)
COPY( short_i8,     int8_t, short)
COPY(   int_i8,     int8_t, int)
COPY(  long_i8,     int8_t, long)
COPY( float_i8,     int8_t, float)
COPY(double_i8,     int8_t, double)

COPY(  char_u8,    uint8_t, char)
COPY( short_u8,    uint8_t, short)
COPY(   int_u8,    uint8_t, int)
COPY(  long_u8,    uint8_t, long)
COPY( float_u8,    uint8_t, float)
COPY(double_u8,    uint8_t, double)

COPY(  char_i16,   int16_t, char)
COPY( short_i16,   int16_t, short)
COPY(   int_i16,   int16_t, int)
COPY(  long_i16,   int16_t, long)
COPY( float_i16,   int16_t, float)
COPY(double_i16,   int16_t, double)

COPY(  char_u16,  uint16_t, char)
COPY( short_u16,  uint16_t, short)
COPY(   int_u16,  uint16_t, int)
COPY(  long_u16,  uint16_t, long)
COPY( float_u16,  uint16_t, float)
COPY(double_u16,  uint16_t, double)

COPY(  char_i32,   int32_t, char)
COPY( short_i32,   int32_t, short)
COPY(   int_i32,   int32_t, int)
COPY(  long_i32,   int32_t, long)
COPY( float_i32,   int32_t, float)
COPY(double_i32,   int32_t, double)

COPY(  char_u32,  uint32_t, char)
COPY( short_u32,  uint32_t, short)
COPY(   int_u32,  uint32_t, int)
COPY(  long_u32,  uint32_t, long)
COPY( float_u32,  uint32_t, float)
COPY(double_u32,  uint32_t, double)

COPY(  char_i64,   int64_t, char)
COPY( short_i64,   int64_t, short)
COPY(   int_i64,   int64_t, int)
COPY(  long_i64,   int64_t, long)
COPY( float_i64,   int64_t, float)
COPY(double_i64,   int64_t, double)

COPY(  char_u64,  uint64_t, char)
COPY( short_u64,  uint64_t, short)
COPY(   int_u64,  uint64_t, int)
COPY(  long_u64,  uint64_t, long)
COPY( float_u64,  uint64_t, float)
COPY(double_u64,  uint64_t, double)

COPY(  char_f32, float32_t, char)
COPY( short_f32, float32_t, short)
COPY(   int_f32, float32_t, int)
COPY(  long_f32, float32_t, long)
COPY( float_f32, float32_t, float)
COPY(double_f32, float32_t, double)

COPY(  char_f64, float64_t, char)
COPY( short_f64, float64_t, short)
COPY(   int_f64, float64_t, int)
COPY(  long_f64, float64_t, long)
COPY( float_f64, float64_t, float)
COPY(double_f64, float64_t, double)
#undef COPY

static copier* copiers[] = {
    copy_char_i8, copy_short_i8, copy_int_i8, copy_long_i8,
    copy_float_i8, copy_double_i8,

    copy_char_u8, copy_short_u8, copy_int_u8, copy_long_u8,
    copy_float_u8, copy_double_u8,

    copy_char_i16, copy_short_i16, copy_int_i16, copy_long_i16,
    copy_float_i16, copy_double_i16,

    copy_char_u16, copy_short_u16, copy_int_u16, copy_long_u16,
    copy_float_u16, copy_double_u16,

    copy_char_i32, copy_short_i32, copy_int_i32, copy_long_i32,
    copy_float_i32, copy_double_i32,

    copy_char_u32, copy_short_u32, copy_int_u32, copy_long_u32,
    copy_float_u32, copy_double_u32,

    copy_char_i64, copy_short_i64, copy_int_i64, copy_long_i64,
    copy_float_i64, copy_double_i64,

    copy_char_u64, copy_short_u64, copy_int_u64, copy_long_u64,
    copy_float_u64, copy_double_u64,

    copy_char_f32, copy_short_f32, copy_int_f32, copy_long_f32,
    copy_float_f32, copy_double_f32,

    copy_char_f64, copy_short_f64, copy_int_f64, copy_long_f64,
    copy_float_f64, copy_double_f64,
};

void ytao_copy(
    void*       dst,
    tao_eltype  dst_type,
    const void* src,
    int         src_type,
    long ntot)
{
    int idx = (src_type - Y_CHAR) +
        (Y_DOUBLE - Y_CHAR + 1)*(dst_type - TAO_INT8);
    copiers[idx](dst, src, ntot);
}
