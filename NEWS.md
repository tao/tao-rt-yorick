# User-visible changes in Yorick interface to TAO

## Version 0.11.0

- Remote objects have member `pid` to retrieve the process identifier of their
  owner.

## Version 0.10.0

- New generic function `tao_attach` to attach to any shared TAO object and
  automatically determine the object type.

- New function `tao_private` to associate any kind of data to a shared TAO
  object.

- Raw encoding and pre-processing can be specified as a string in
  `tao_configure`.   For instance:

  ```
  tao_configure, cam, 1.0, preprocessing="affine", pixeltype=float,
      rawencoding="Mono(12,24)";
  ```

- New functions `tao_format_encoding`, `tao_parse_encoding`,
  `tao_format_preprocessing`, and `tao_parse_preprocessing` to format and parse
  encoding and pre-processing parameters.

- Camera configuration settings `bufferencoding` and `sensorencoding` have been
  merged in a single parameter `rawencoding` to choose the encoding of pixels
  in raw acquired images.

- Named remote camera attributes have been implemented.


## Version 0.9.0

- The actuators commands sent to deformable mirrors are the sum of 3 terms: the
  reference commands, some given perturbation commands, and the requested
  commands.  The perturbations are only valid for the next commands sent to the
  deformable mirror.  The reference commands are valid for the subsequent
  commands sent to the mirror until a new reference commands are set.  A
  data-frame from the mirror telemetry contains all relevant information (see
  doc. for `tao_wait_frame`).

- Functions like `tao_send_commands`, `tao_set_reference`, and
  `tao_set_perturbation` yield a pair of serial numbers in the form of a
  lightweight object.

- Support for remote (wavefront) sensors.


## Version 0.8.2

- For a remote mirror, `dm.cmin` and `dm.cmax` yield the minimum and maximum
  possible command values.

- For a shared array, `arr.timestamps` yields a vector of timestamps.


# Version 0.8.0

- Image counter(s) have been replaced by serial number(s).  Accordingly, object
  member `counter` has been renamed as `serial` and the functions
  `tao_get_counter` and `tao_set_counter` have been renamed `tao_get_serial`
  and `tao_set_serial`.  The function `tao_get_last_image_counter` has been
  replaced by `tao_get_serial` which can take any kind of TAO object as
  argument.
