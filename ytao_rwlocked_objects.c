// ytao_rwlocked_object.c -
//
// Yorick interface to TAO r/w locked objects.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2018-2022, Éric Thiébaut <eric.thiebaut@univ-lyon1.fr>
//
// See LICENSE.md for details.

#include "ytao.h"
#include "macros.h"

#include <tao-rwlocked-objects-private.h>

ytao_operations ytao_rwlocked_object_ops = {
    // TAO_RWLOCKED_OBJECT,
    true,
    NULL, // on_eval
    NULL, // on_eval
    NULL, // on_wait_data_frame
};
